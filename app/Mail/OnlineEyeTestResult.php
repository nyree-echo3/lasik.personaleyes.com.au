<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Setting;

class OnlineEyeTestResult extends Mailable
{
    use Queueable, SerializesModels;

    public $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function build()
    {

        $setting = Setting::where('key','=','contact-email')->first();
        $contactEmail = $setting->value;

        return $this->subject('PersonalEYES Online Eye Test Result')
            ->from($contactEmail)
            ->view('site/emails/online-eye-test-result');
    }
}
