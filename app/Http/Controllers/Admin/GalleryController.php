<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\NavigationHelper;
use App\Http\Controllers\Controller;
use App\Module;
use App\Images;
use App\SpecialUrl;
use App\GalleryCategory;
use Illuminate\Http\Request;
use Validator, Illuminate\Support\Facades\Input, Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class GalleryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
		
		$module_details = Module::where('slug', '=', 'gallery')->first();            
		view()->share('display_name', $module_details->display_name);
    }

    public function index(Request $request)
    {
        $is_filtered = $this->isFiltered($request);
        $paginate_count = session()->get('pagination-count');

        if ($is_filtered) {
            $images = Images::Filter()->orderBy('position', 'desc')->paginate($paginate_count);
        } else {
            $images = Images::with('category')->orderBy('position', 'desc')->paginate($paginate_count);
        }

        $session = session()->get('gallery-filter');
        $categories = GalleryCategory::orderBy('created_at', 'desc')->get();
        return view('admin/gallery/gallery', array(
            'images' => $images,
            'categories' => $categories,
            'is_filtered' => $is_filtered,
            'session' => $session
        ));
    }

    public function add()
    {
        $categories = GalleryCategory::orderBy('created_at', 'desc')->get();
        return view('admin/gallery/add', array(
            'categories' => $categories
        ));
    }

    public function edit($image_id)
    {
        $image = Images::where('id', '=', $image_id)->first();
        $categories = GalleryCategory::orderBy('created_at', 'desc')->get();
        return view('admin/gallery/edit', array(
            'image' => $image,
            'categories' => $categories
        ));
    }

    public function store(Request $request)
    {
        $rules = array(
            'category_id' => 'required',
            'name' => 'required',           
            'location' => 'required',
        );

        $messages = [
            'category_id.required' => 'Please select category',
            'name.required' => 'Please enter name',           
            'location.required' => 'Please select an image',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/gallery/add')->withErrors($validator)->withInput();
        }

        $image = new Images();
        $image->category_id = $request->category_id;
        $image->name = $request->name;
        $image->location = $request->location;
        $image->description = $request->description;

        if($request->live=='on'){
           $image->status = 'active'; 
        }

        $image->position = Images::max('position')+1;
        $image->save();
      
		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/gallery/' . $image->id . '/edit')->with('message', Array('text' => 'Item has been added', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/gallery/')->with('message', Array('text' => 'Item has been added', 'status' => 'success'));
		}		        


    }

    public function update(Request $request)
    {
        $rules = array(
            'category_id' => 'required',
            'name' => 'required',
            'location' => 'required'
        );

        $messages = [
            'category_id.required' => 'Please select category',
            'name.required' => 'Please enter name',
            'location.required' => 'Please select an image'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/gallery/' . $request->id . '/edit')->withErrors($validator)->withInput();
        }

        $image = Images::where('id','=',$request->id)->first();
        $image->category_id = $request->category_id;
        $image->name = $request->name;
		$image->location = $request->location;
        $image->description = $request->description;
		if($request->live=='on'){
           $image->status = 'active'; 
		} else {
			$image->status = 'passive';
        }
        $image->save();
       
		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/gallery/' . $image->id . '/edit')->with('message', Array('text' => 'Item has been updated', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/gallery/')->with('message', Array('text' => 'Item has been updated', 'status' => 'success'));
		}
    }

    public function delete($image_id)
    {
        $news = Images::find($image_id);
        $news->delete();

        return \Redirect::back()->with('message', Array('text' => 'Item has been deleted.', 'status' => 'success'));
    }

    public function changeStatus(Request $request, $image_id)
    {
        $image = Images::where('id', '=', $image_id)->first();
        if ($request->status == "true") {
            $image->status = 'active';
        } else if ($request->status == "false") {
            $image->status = 'passive';
        }
        $image->save();

        return Response::json(['status' => 'success']);
    }

    public function categories()
    {
        $categories = GalleryCategory::orderBy('position', 'desc')->get();
        return view('admin/gallery/categories', compact('categories'));
    }

    public function addCategory()
    {
        return view('admin/gallery/add-category');
    }

    public function storeCategory(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'slug' => 'required|unique_store:gallery_categories',
            'special_url' => 'nullable|regex:/^[\w-]*$/|special_url_store',
        );

        $messages = [
            'name.required' => 'Please enter category name.',
            'slug.unique_store' => 'Seo name is in use',
            'special_url.regex' => 'Only alphanumeric characters, dashes and underscores allowed',
            'special_url.special_url_store' => 'The url is already taken'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/gallery/add-category')->withErrors($validator)->withInput();
        }

        $category = new GalleryCategory();
        $category->name = $request->name;
        $category->slug = $request->slug;
		$category->description = $request->description;
        if($request->live=='on'){
            $category->status = 'active';
        }
        $category->save();

        ///////////special URL//
        if($request->special_url!=""){
            $new_special_url = new SpecialUrl();
            $new_special_url->item_id = $category->id;
            $new_special_url->module = 'gallery';
            $new_special_url->type = 'category';
            $new_special_url->url = $request->special_url;
            $new_special_url->save();

        }
        ////////////////////////

        (new NavigationHelper())->navigationAddItem($request->name, $category->url, 'category-'.$category->id, 'gallery', 'category', $category->slug);

		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/gallery/' . $category->id . '/edit-category')->with('message', Array('text' => 'Category has been added', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/gallery/categories')->with('message', Array('text' => 'Category has been added', 'status' => 'success'));
		}

    }

    public function editCategory($category_id)
    {
        $category = GalleryCategory::where('id', '=', $category_id)->first();

        $category->special_url = "";
        $special_url = SpecialUrl::where('item_id','=',$category->id)->where('module','=','gallery')->where('type','=','category')->first();
        if($special_url){
            $category->special_url = $special_url->url;
        }

        return view('admin/gallery/edit-category', array(
            'category' => $category,
        ));
    }

    public function updateCategory(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'slug' => 'required|unique_update:gallery_categories,'.$request->id,
            'special_url' => 'nullable|regex:/^[\w-]*$/|special_url_update:gallery,category,' . $request->id,
        );

        $messages = [
            'name.required' => 'Please enter category name.',
            'slug.unique_update' => 'Seo name is in use',
            'special_url.regex' => 'Only alphanumeric characters, dashes and underscores allowed',
            'special_url.special_url_update' => 'The url is already taken'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/gallery/' . $request->id . '/edit-category')->withErrors($validator)->withInput();
        }

        $category = GalleryCategory::findOrFail($request->id);

        (new NavigationHelper())->navigationItems('update', 'category', 'gallery', $category->slug, null, $request->name, $request->slug);

        $category->name = $request->name;
        $category->slug = $request->slug;
		$category->description = $request->description;
        if($request->live=='on'){
           $category->status = 'active';

            (new NavigationHelper())->navigationAddItem($category->name, $category->url, 'category-'.$category->id, 'gallery', 'category', $category->slug);
		} else {
			$category->status = 'passive';
            (new NavigationHelper())->navigationItems('delete-item', 'category', 'gallery', $category->slug);
        }
        $category->save();

        (new NavigationHelper())->navigationItems('change-href', 'category', 'gallery', $category->slug, $category->url);

        ///////////special URL//
        $special_url = SpecialUrl::where('item_id', '=', $category->id)->where('module', '=', 'gallery')->where('type', '=', 'category')->first();

        if ($special_url) {

            if ($request->special_url != "") {
                $special_url->url = $request->special_url;
                $special_url->save();

                (new NavigationHelper())->navigationItems('change-href', 'category', 'gallery', $category->slug, $request->special_url);

            } else {
                $special_url->delete();
                (new NavigationHelper())->navigationItems('change-href', 'category', 'gallery', $category->slug, 'gallery/'.$category->slug);
            }
        } else {

            if ($request->special_url) {

                $new_special_url = new SpecialUrl();
                $new_special_url->item_id = $category->id;
                $new_special_url->module = 'gallery';
                $new_special_url->type = 'category';
                $new_special_url->url = $request->special_url;
                $new_special_url->save();

                (new NavigationHelper())->navigationItems('change-href', 'category', 'gallery', $category->slug, $request->special_url);
            }
        }
        ////////////////////////

		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/gallery/' . $category->id . '/edit-category')->with('message', Array('text' => 'Category has been updated', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/gallery/categories')->with('message', Array('text' => 'Category has been updated', 'status' => 'success'));
		}
    }

    public function deleteCategory($category_id)
    {
        $category = GalleryCategory::where('id','=',$category_id)->first();

        if(count($category->images)){
            return \Redirect::to('dreamcms/gallery/categories')->with('message', Array('text' => 'Category has images. Please delete images first.', 'status' => 'error'));
        }

        $category->is_deleted = true;
        $category->save();

        SpecialUrl::where('item_id','=',$category->id)->where('module','=','gallery')->where('type','=','category')->delete();

        (new NavigationHelper())->navigationItems('delete-item', 'category', 'gallery', $category->slug);

        return \Redirect::to('dreamcms/gallery/categories')->with('message', Array('text' => 'Category has been deleted.', 'status' => 'success'));
    }

    public function changeCategoryStatus(Request $request, $category_id)
    {
        $category = GalleryCategory::where('id', '=', $category_id)->first();
        if ($request->status == "true") {
            $category->status = 'active';

            (new NavigationHelper())->navigationAddItem($category->name, $category->url, 'category-'.$category->id, 'gallery', 'category', $category->slug);

        } else if ($request->status == "false") {
            $category->status = 'passive';

            (new NavigationHelper())->navigationItems('delete-item', 'category', 'gallery', $category->slug);
        }
        $category->save();

        return Response::json(['status' => 'success']);
    }

    public function sortCategory()
    {
        $categories = GalleryCategory::where('status','=','active')->orderBy('position', 'desc')->get();

        return view('admin/gallery/sort-category', array(
            'categories' => $categories
        ));
    }

    public function imageSort(Request $request)
    {
        $image_count = count($request->item);

        foreach($request->item as $image_id){
            $image = Images::where('id','=',$image_id)->first();
            $image->position = $image_count;
            $image->save();
            $image_count--;
        }
        return Response::json(['status' => 'success']);
    }

    public function emptyFilter()
    {
        session()->forget('gallery-filter');
        return redirect()->to('dreamcms/gallery');
    }

    public function isFiltered($request)
    {

        $filter_control = false;

        if ($request->category && $request->category != "all") {
            $filter_control = true;
        }

        if ($request->search) {
            $filter_control = true;
        }

        if ($filter_control) {
            $request->session()->put('gallery-filter', [
                'category' => $request->category,
                'search' => $request->search
            ]);
        }

        if (session()->has('gallery-filter')) {
            $filter_control = true;
        }

        return $filter_control;
    }

}