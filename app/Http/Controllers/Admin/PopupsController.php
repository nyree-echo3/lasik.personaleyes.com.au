<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Module;
use App\Popup;
use App\Setting;

use Illuminate\Http\Request;
use Validator, Illuminate\Support\Facades\Input, Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class PopupsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
		
		$module_details = Module::where('slug', '=', 'popups')->first();            
		view()->share('display_name', $module_details->display_name);
    }

    public function index(Request $request)
    {       
        $paginate_count = session()->get('pagination-count');

        $popups = Popup::orderBy('id', 'desc')->paginate($paginate_count);
       
        return view('admin/popups/popups', array(
            'popups' => $popups,            
        ));
    }

    public function add()
    {        
        return view('admin/popups/add', array(           
        ));
    }

    public function edit($popup_id)
    {
        $popup = Popup::where('id', '=', $popup_id)->first();    
		
        return view('admin/popups/edit', array(
            'popup' => $popup,           
        ));
    }
	
    public function store(Request $request)
    {		
        $rules = array(            
            'title' => 'required',     
			'form' => 'required',          
        );

        $messages = [           
            'title.required' => 'Please enter title',    
			'form.required' => 'Please enter form',            
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/popups/add')->withErrors($validator)->withInput();
        }

        $popup = new Popup();       
		$popup->title = $request->title;	
        $popup->form = $request->form;		
		$popup->image = $request->image;		
		$popup->popup_position = $request->position;
		
        if ($request->live == 'on') {
            $popup->status = 'active';
        }

		
        $popup->save();
   
		if ($request->get('action') == 'save') {
            return \Redirect::to('dreamcms/popups/' . $popup->id . '/edit')->with('message', Array('text' => 'Item has been added', 'status' => 'success'));
        } else {
            return \Redirect::to('dreamcms/popups/')->with('message', Array('text' => 'Item has been added', 'status' => 'success'));
        }    
    }

    public function update(Request $request)
    {				
        $rules = array(           
            'title' => 'required',     
			'form' => 'required',  
        );

        $messages = [        
            'title.required' => 'Please enter title',    
			'form.required' => 'Please enter form',    
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return Response::json([
                'status' => 'fail',
                'message' => 'Please enter title'
            ]);
        }

        $popup = Popup::where('id','=',$request->id)->first();       
        $popup->title = $request->title;	
        $popup->form = $request->form;		
		$popup->image = $request->image;
		$popup->popup_position = $request->position;
		
		if ($request->live == 'on') {
            $popup->status = 'active';
        } else {
            $popup->status = 'passive';
        }
		
        $popup->save();
		
        if ($request->get('action') == 'save') {
            return \Redirect::to('dreamcms/popups/' . $popup->id . '/edit')->with('message', Array('text' => 'Item has been updated', 'status' => 'success'));
        } else {
            return \Redirect::to('dreamcms/popups/')->with('message', Array('text' => 'Item has been updated', 'status' => 'success'));
        }
    }

    public function delete($popup_id)
    {
        $popup = Popup::where('id','=',$popup_id)->first();
        $popup->is_deleted = true;
        $popup->save();

        return \Redirect::back()->with('message', Array('text' => 'Item has been deleted.', 'status' => 'success'));
    }

    public function changeCtaStatus(Request $request, $popup_id)
    {
        $popup = Popup::where('id', '=', $popup_id)->first();
        if ($request->status == "true") {
            $popup->status = 'active';
        } else if ($request->status == "false") {
            $popup->status = 'passive';
        }
        $popup->save();

        return Response::json(['status' => 'success']);
    }    
}