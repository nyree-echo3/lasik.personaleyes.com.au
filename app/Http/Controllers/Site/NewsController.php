<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\News;
use App\NewsCategory;
use App\Module;
use App\Cta;
use App\Popup;

class NewsController extends Controller
{
    public function list(Request $request, $category_slug = "", $item_slug = ""){    
		$module = Module::where('slug', '=', "news")->first();
		
    	$side_nav = $this->getCategories();	
		
		if ($category_slug == "")  {
		   // Get Latest News
		   $category_name = "Articles";	
			
		   $items = $this->getNews();
		} elseif ($category_slug != "" && $item_slug == "") {
		  // Get Category News	
		  $category = $this->getCategory($category_slug);
		  $category_name = $category->name;	
			
		  $items = $this->getNews($category->id);			  
		} 		
		
		return view('site/news/list', array(   
			'module' => $module,
			'side_nav' => $side_nav,
			'category_name' => $module->display_name,
			'items' => $items,				
        ));

    }
	
    public function item($category_slug, $item_slug, $mode = ""){
		$module = Module::where('slug', '=', "news")->first();
		$today = date("Y/m/d");		
		
    	$side_nav = $this->getCategories();				  			
		$news_item = $this->getNewsItem($item_slug, $mode);		
		
		$news_additional = News::inRandomOrder()->where('id', '<>', $news_item->id)->where('status', '=', 'active')->where('start_date', '<=', $today)->where('archive_date', '>', $today)->paginate(2);
		
		$cta_side = Cta::where('id', '=', $news_item->cta_side)->first();
		$cta_bottom = Cta::where('id', '=', $news_item->cta_bottom)->first();
		
		$popup = Popup::where('id', '=', $news_item->popup_type)->first(); 
		
		return view('site/news/item', array(  
			'module' => $module,
			'side_nav' => $side_nav,					
			'news_item' => $news_item,	
			'mode' => $mode,
			'news_additional' => $news_additional,
			'cta_side' => $cta_side,
			'cta_bottom' => $cta_bottom,
			'category_name' => $module->display_name,
			'popup' => $popup
        ));
    }	
	
	public function archive(Request $request, $age = ""){   
		$module = Module::where('slug', '=', "news")->first();
		$category_name = "Archived News";	
		
    	//$side_nav = $this->getCategories();	
				
		// Get Archived News		
		$items = $this->getNewsArchive($age);			
		
		return view('site/news/archive', array( 
			'module' => $module,
			'age' => $age,	
			'items' => $items,	
			'category_name' => $category_name,
        ));

    }
	
	public function getCategories(){
		$categories = NewsCategory::whereHas("news")->where('status', '=', 'active')->get();		
		return($categories);
	}	
	
	public function getNews($category_id = "", $limit = 100){
		$today = date("Y/m/d");		
		
		if ($category_id == "")  {
			$news = News::where('status', '=', 'active')
						->where('start_date', '<=', $today)
						->where('archive_date', '>', $today)
						->orderBy('start_date', 'desc')
						->paginate($limit);	
		} else {
		   $news = News::where('status', '=', 'active')
			            ->where('category_id', '=', $category_id)
						->where('start_date', '<=', $today)
						->where('archive_date', '>', $today)
						->orderBy('start_date', 'desc')
						->paginate($limit);		
		}
		
		return($news);
	}
	
	public function getNewsArchive($age = 6){
		$today = date("Y/m/d");		
		$start_period = $today;
		
//		switch ($age)  {
//			case 0:
//		        $start_period = $today;
//		        $end_period = date("Y-m-d", strtotime($today . ' -6 months'));
//				break;
//			
//			case 6:
//		        $start_period = date("Y-m-d", strtotime($today  . ' -6 months'));
//		        $end_period = date("Y-m-d", strtotime($start_period . ' -6 months'));
//				break;
//				
//			case 12:
//		        $start_period = date("Y-m-d", strtotime($today  . ' -12 months'));
//		        $end_period = date("Y-m-d", strtotime($start_period . ' -12 months'));
//				break;
//			
//			case 24:
//		        $start_period = date("Y-m-d", strtotime($today  . ' -24 months'));
//		        $end_period = date("Y-m-d", strtotime($start_period . ' -12 months'));
//				break;
//			
//			case 36:
//		        $start_period = date("Y-m-d", strtotime($today  . ' -36 months'));
//		        $end_period = date("Y-m-d", strtotime($start_period . ' -100 years'));
//				break;
//				
//		}				
		
		$news = News::whereHas("categoryactive")
			        ->where('status', '=', 'active')						
					->where('archive_date', '<=', $start_period)
			       // ->where('archive_date', '>', $end_period)
			        ->where('status', '=', 'active')
					->orderBy('start_date', 'desc')
					->paginate(2);
		;
		
		return($news);
	}
	  
	public function getNewsItem($item_slug, $mode){		
		if ($mode == "preview") {
		   $news = News::where(['slug' => $item_slug])->first();							
		} else {
		   $news = News::where(['status' => 'active', 'slug' => $item_slug])->first();						
		}
		return($news);
	}
	
	public function getCategory($category_slug){
		$categories = NewsCategory::where('slug', '=', $category_slug)->first();		
		return($categories);
	}		
	
    public function success(){
		$module = Module::where('slug', '=', "news")->first();
		 
		$side_nav = $this->getCategories();					
		$category_name = "Latest News";				
		$items = $this->getNews();
		
        return view('site/news/success', array(  
			'module' => $module, 
			'side_nav' => $side_nav,					
			'news_item' => $items,				
        ));
    }	
}
