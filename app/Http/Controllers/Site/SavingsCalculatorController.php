<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;

class SavingsCalculatorController extends Controller
{
    public function index()
    {
        return view('site/savings-calculator/calculator');
    }
}
