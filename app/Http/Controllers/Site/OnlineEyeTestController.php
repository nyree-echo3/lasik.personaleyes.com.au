<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Mail\OnlineEyeTestResult;
use Illuminate\Http\Request;
use Torann\GeoIP\Facades\GeoIP;
use Illuminate\Support\Facades\Mail;

class OnlineEyeTestController extends Controller
{
    public function step1()
    {

        $country = geoip()->getLocation(request()->ip())->country;
        $country = 'Australia';
        session()->put('country', $country);
        session()->put('counter', 'Please try again!');

        $random_string = $this->random_gen(8);

        return view('site/online-eye-test/step1',array(
            'random_string' => $random_string
        ));
    }

    public function step2(Request $request)
    {
        $uri_parts = explode('/', url()->previous());
        $last_segment = end($uri_parts);
        if($last_segment!='step1'){
            return redirect('online-eye-test/step1');
        }

        if(strtoupper($request->step1input) == $request->chars){

            session()->put('counter', 'It\'s either time to see an eye health professional, or time to re-read the instructions. Make sure you\'re sitting no more than 2 metres from your computer screen. If this is the case and you still can\'t read, your vision is approximately 6/21, or 20/70 in imperial measurements.');

            $random_string = $this->random_gen(8);

            return view('site/online-eye-test/step2',array(
                'random_string' => $random_string
            ));
        }

        return $this->result();
    }

    public function step3(Request $request)
    {

        $uri_parts = explode('/', url()->previous());
        $last_segment = end($uri_parts);
        if($last_segment!='step2'){
            return redirect('online-eye-test/step1');
        }

        if(strtoupper($request->step2input) == $request->chars){

            session()->put('counter', 'your visual acuity is 6/18 (20/60 in the imperial system). This means your vision loss is low to moderate, and you should look into seeing an eye health professional for prescription visual aids, or to discover whether you would make a good candidate for laser eye surgery.');

            $random_string = $this->random_gen(8);

            return view('site/online-eye-test/step3',array(
                'random_string' => $random_string
            ));
        }

        return $this->result();
    }

    public function step4(Request $request)
    {

        $uri_parts = explode('/', url()->previous());
        $last_segment = end($uri_parts);
        if($last_segment!='step3'){
            return redirect('online-eye-test/step1');
        }

        if(strtoupper($request->step3input) == $request->chars){

            session()->put('counter', 'You have mild vision loss, giving you visual acuity of 6/15, or 20/50. While this will generally require glasses, it should not affect your lifestyle too much once the prescription is acquired.  Want sharper vision without glasses or contacts? Talk to us about how laser vision correction can help you see clearly.');

            $random_string = $this->random_gen(8);

            return view('site/online-eye-test/step4',array(
                'random_string' => $random_string
            ));
        }

        return $this->result();
    }

    public function step5(Request $request)
    {

        $uri_parts = explode('/', url()->previous());
        $last_segment = end($uri_parts);
        if($last_segment!='step4'){
            return redirect('online-eye-test/step1');
        }

        if(strtoupper($request->step4input) == $request->chars){

            session()->put('counter', 'To drive without glasses on, this is the minimum you\'ll need to achieve. Your visual acuity is 6/12, or 20/40 in the old measurements');

            $random_string = $this->random_gen(8);

            return view('site/online-eye-test/step5',array(
                'random_string' => $random_string
            ));
        }

        return $this->result();
    }

    public function step6(Request $request)
    {

        $uri_parts = explode('/', url()->previous());
        $last_segment = end($uri_parts);
        if($last_segment!='step5'){
            return redirect('online-eye-test/step1');
        }

        if(strtoupper($request->step5input) == $request->chars){

            session()->put('counter', 'Reading up to the fifth step means you have visual acuity of 6/9, or 20/30. This is the level you need to reach in at least one eye in order to drive a heavy vehicle.');

            $random_string = $this->random_gen(8);

            return view('site/online-eye-test/step6',array(
                'random_string' => $random_string
            ));
        }

        return $this->result();
    }

    public function endTest(Request $request)
    {

        if(strtoupper($request->step6input) == $request->chars){
            session()->put('counter', 'Congratulations if you could read up to step six: this gives you \'normal\' vision of 6/6, or 20/20. Despite popular belief, a score of 6/6 doesn\'t equal \'perfect\' vision, since it is both possible and common to see better than 6/6. In fact, it\'s believed that the human eye has a maximum acuity of 6/3 without visual aids such as binoculars. To fly a plane, you\'ll need to either have 6/6 vision or have your vision corrected to 6/6.');
        }

        return $this->result();
    }

    public function result()
    {

        if(session()->get('country')=='Australia'){
            return view('site/online-eye-test/result');
        }

        return view('site/online-eye-test/direct-result');
    }

    public function email()
    {
        return view('site/online-eye-test/email');
    }

    public function sendResult(Request $request)
    {
        Mail::to($request->email)->send(new OnlineEyeTestResult(session()->get('counter')));
    }

    public function feedback()
    {
        return view('site/online-eye-test/feedback');
    }

    private function random_gen($length)
    {
        $booom = str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
        $random = substr($booom, 18);
        return $random;
    }
}
