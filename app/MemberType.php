<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Rutorika\Sortable\SortableTrait;

class MemberType extends Model
{
    use SortableTrait;

    protected $table = 'member_types';

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function members()
    {
        return $this->hasMany(Member::class, 'type_id');
    }
}
