<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cta extends Model
{  

    protected $table = 'ctas';
   
    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }   
}
