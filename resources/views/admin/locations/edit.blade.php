@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/iCheck/all.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/locations') }}"><i class="fas fa-map-marker-alt"></i> {{ $display_name }}</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit</h3>
                        </div>

                        <form method="post" class="form-horizontal" action="{{ url('dreamcms/locations/update') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="id" value="{{ $location->id }}">
                            <div class="box-body">
                                <div class="form-group{{ ($errors->has('name')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Name *</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="name" id="name" placeholder="Name"
                                               value="{{ old('name',$location->name) }}">
                                        @if ($errors->has('name'))
                                            <small class="help-block">{{ $errors->first('name') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('slug')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">SEO Name *</label>
                                    <div class="col-sm-10">

                                        <div class="input-group">
                                            <input type="text" id="slug" name="slug" class="form-control"
                                                   value="{{ old('slug',$location->slug) }}" readonly>
                                            <span class="input-group-btn">
                                              <button type="button" class="btn btn-flat btn-info" data-toggle="modal"
                                                      data-target="#change-slug">Change SEO Name
                                              </button>
                                            </span>
                                        </div>

                                        @if ($errors->has('slug'))
                                            <small class="help-block">{{ $errors->first('slug') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('description')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Description</label>

                                    <div class="col-sm-10">
                                        <textarea class="form-control" rows="3" name="description"
                                                  placeholder="Description">{{ old('description', $location->description) }}</textarea>
                                        @if ($errors->has('description'))
                                            <small class="help-block">{{ $errors->first('description') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ ($errors->has('address')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Address *</label>

                                    <div class="col-sm-10">
                                        <textarea class="form-control" name="address" placeholder="Address">{{ old('address',$location->address) }}</textarea>
                                        @if ($errors->has('address'))
                                            <small class="help-block">{{ $errors->first('address') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <!--<div class="form-group{{ ($errors->has('address2')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label"></label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="address2" placeholder="Address (Line 2)"
                                               value="{{ old('address2',$location->address2) }}">
                                        @if ($errors->has('address2'))
                                            <small class="help-block">{{ $errors->first('address2') }}</small>
                                        @endif
                                    </div>
                                </div>-->
                                
                                <div class="form-group{{ ($errors->has('suburb')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Suburb *</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="suburb" placeholder="Suburb"
                                               value="{{ old('suburb',$location->suburb) }}">
                                        @if ($errors->has('suburb'))
                                            <small class="help-block">{{ $errors->first('suburb') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                @php
                                    if(old('state')!=''){
                                        $state = old('state');
                                    }else{
                                        $state = $location->state;
                                    }
                                @endphp
                                <div class="form-group{{ ($errors->has('state')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">State *</label>

                                    <div class="col-sm-10">
                                        <select name="state" class="form-control select2" style="width: 100%;">
                                            <option value="ACT"{{ ($state == "ACT") ? ' selected="selected"' : '' }}>
                                                ACT
                                            </option>
                                            <option value="QLD"{{ ($state == "QLD") ? ' selected="selected"' : '' }}>
                                                QLD
                                            </option>
                                            <option value="NSW"{{ ($state == "NSW") ? ' selected="selected"' : '' }}>
                                                NSW
                                            </option>
                                            <option value="NT"{{ ($state == "NT") ? ' selected="selected"' : '' }}>
                                                NT
                                            </option>
                                            <option value="SA"{{ ($state == "SA") ? ' selected="selected"' : '' }}>
                                                SA
                                            </option>
                                            <option value="TAS"{{ ($state == "TAS") ? ' selected="selected"' : '' }}>
                                                TAS
                                            </option>
                                            <option value="VIC"{{ ($state == "VIC") ? ' selected="selected"' : '' }}>
                                                VIC
                                            </option>
                                            <option value="WA"{{ ($state == "WA") ? ' selected="selected"' : '' }}>
                                                WA
                                            </option>
                                            <option value="OTHER"{{ ($state == "OTHER") ? ' selected="selected"' : '' }}>
                                                OTHER
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group{{ ($errors->has('postcode')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Postcode *</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="postcode" placeholder="Postcode"
                                               value="{{ old('postcode',$location->postcode) }}">
                                        @if ($errors->has('postcode'))
                                            <small class="help-block">{{ $errors->first('postcode') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ ($errors->has('country')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Country *</label>

                                    <div class="col-sm-10">
                                        <select name="country" class="form-control select2" style="width: 100%;">
                                            <option value="Australia"{{ (old('country') == "Australia") ? ' selected="selected"' : '' }}>
                                                Australia
                                            </option>                                                                                  
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group{{ ($errors->has('phone')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Phone</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="phone" placeholder="Phone"
                                               value="{{ old('phone',$location->phone) }}">
                                        @if ($errors->has('phone'))
                                            <small class="help-block">{{ $errors->first('phone') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ ($errors->has('mobile')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Mobile</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="mobile" placeholder="Mobile"
                                               value="{{ old('mobile',$location->mobile) }}">
                                        @if ($errors->has('mobile'))
                                            <small class="help-block">{{ $errors->first('mobile') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ ($errors->has('fax')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Fax</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="fax" placeholder="Fax"
                                               value="{{ old('fax',$location->fax) }}">
                                        @if ($errors->has('fax'))
                                            <small class="help-block">{{ $errors->first('fax') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ ($errors->has('email')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Email</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="email" placeholder="Email"
                                               value="{{ old('email',$location->email) }}">
                                        @if ($errors->has('email'))
                                            <small class="help-block">{{ $errors->first('email') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ ($errors->has('website')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Google Map - Website Link</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="website" placeholder="Website"
                                               value="{{ old('website',$location->website) }}">
                                        @if ($errors->has('website'))
                                            <small class="help-block">{{ $errors->first('website') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ ($errors->has('map')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Google Map - Embed</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="map" placeholder="Google Map"
                                               value="{{ old('map',$location->map) }}">
                                        @if ($errors->has('map'))
                                            <small class="help-block">{{ $errors->first('map') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('directions')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Directions</label>

                                    <div class="col-sm-10">
                                        <textarea class="form-control" rows="3" name="directions"
                                                  placeholder="Directions">{{ old('directions', $location->directions) }}</textarea>
                                        @if ($errors->has('directions'))
                                            <small class="help-block">{{ $errors->first('directions') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('fileName')) ? ' has-error' : '' }}">
                                <label class="col-sm-2 control-label">File</label>
                                <div class="col-sm-10">
                                    <input type="hidden" id="fileName" name="fileName"
                                           value="{{ old('fileName',$location->fileName) }}">
                                    <button id="document-popup" type="button" class="btn btn-info btn-sm">Upload
                                        Document
                                    </button>
                                    @php
                                        $class = ' invisible';
                                        if(old('fileName',$location->fileName)){
                                            $class = '';
                                        }
                                    @endphp
                                    <button id="remove-document" type="button"
                                            class="btn btn-danger btn-sm{{ $class }}">Remove Document
                                    </button>
                                    <br/><br/>
                                    <span id="added_document">
                                        @if( old('fileName',$location->fileName))
                                            <a href='{{ url('dreamcms/').$location->fileName }}'
                                               target='_blank'>{{ url('dreamcms/').$location->fileName }}</a>
                                        @endif
                                        </span>
                                    @if ($errors->has('fileName'))
                                        <small class="help-block">{{ $errors->first('fileName') }}</small>
                                    @endif
                                </div>
                            </div>
                                
                               
                                
                            </div>
                            @php
                                if(count($errors)>0){
                                   if(old('live')=='on'){
                                    $status = 'active';
                                   }else{
                                    $status = '';
                                   }
                                }else{
                                    $status = $location->status;
                                }
                            @endphp
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Status *</label>
                                <div class="col-sm-10">
                                    <label>
                                        <input class="page_status" type="checkbox" data-toggle="toggle" data-size="mini"
                                               name="live" {{ $status == 'active' ? ' checked' : '' }}>
                                    </label>
                                </div>
                            </div>
                            
                            <hr>							
                           
                            <div class="form-group{{ ($errors->has('popup_type')) ? ' has-error' : '' }}">
								<label class="col-sm-2 control-label">Pop Up</label>

								<div class="col-sm-10">
									<select name="popup_type" class="form-control" data-placeholder="None" style="width: 100%;">
										<option value=""></option>
										@foreach($popups as $popup)
											<option value="{{ $popup->id }}" {{ (old('popup_type',$location->popup_type) == $popup->id ? " selected" : "") }}>{{ $popup->title }}</option>
										@endforeach
									</select>
								</div>
							</div>
                              
                            <div class="form-group{{ ($errors->has('popup_position')) ? ' has-error' : '' }}">
								<label class="col-sm-2 control-label">Pop Up Position</label>

								<div class="col-sm-10">
									<select name="popup_position" class="form-control" data-placeholder="None" style="width: 100%;">
                                      	   <option value=""></option>
                                       	   <option value="top-left" {{ ($location->popup_position == 'top-left' ? ' selected' : '') }}>Top Left</option>
                                       	   <option value="top-right" {{ ($location->popup_position == 'top-right' ? ' selected' : '') }}>Top Right</option>
                                       	   <option value="bottom-left" {{ ($location->popup_position == 'bottom-left' ? ' selected' : '') }}>Bottom Left</option>
                                       	   <option value="bottom-right" {{ ($location->popup_position == 'bottom-right' ? ' selected' : '') }}>Bottom Right</option>
                                       </select>
								</div>
							</div>
                               
                            <div class="form-group{{ ($errors->has('popup_start')) ? ' has-error' : '' }}">
								<label class="col-sm-2 control-label">Pop Up - Start (minutes)</label>

								<div class="col-sm-10">
									<input type="number" class="form-control" id="popup_start" name="popup_start" placeholder="Pop Up - Start (minutes)" value="{{ old('popup_start',$location->popup_start) }}" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))" >
									@if ($errors->has('popup_start'))
										<small class="help-block">{{ $errors->first('popup_start') }}</small>
									@endif
								</div>
							</div> 
                               
                            <div class="form-group{{ ($errors->has('popup_end')) ? ' has-error' : '' }}">
								<label class="col-sm-2 control-label">Pop Up - End (minutes)</label>

								<div class="col-sm-10">
									<input type="number" class="form-control" id="popup_end" name="popup_end" placeholder="Pop Up - Start (minutes)" value="{{ old('popup_end',$location->popup_end) }}" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))" >
									@if ($errors->has('popup_end'))
										<small class="help-block">{{ $errors->first('popup_end') }}</small>
									@endif
								</div>
							</div> 

                            <div class="box-footer">
                                <a href="{{ url('dreamcms/locations') }}" class="btn btn-info pull-right"
                                   data-toggle=confirmation data-title="Your changes will be lost! Are you sure?"
                                   data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                                   data-btn-cancel-label="No">Cancel</a>
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save_close">
                                    Save & Close
                                </button>
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save">Save
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </div>
    
    <div class="modal fade" id="change-slug">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Change SEO Name</h4>
                </div>
                <div class="modal-body">
                    <input type="text" class="form-control" id="slug-modal" name="slug-modal"
                           value="{{ old('slug',$location->slug) }}">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" id="save-seo" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/theme/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".select2").select2();

            CKEDITOR.replace('description');
			CKEDITOR.replace('directions');
			CKEDITOR.replace('address');

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });
			
			$('#name').keyup(function () {				
                var slug = convertToKebabCase($(this).val().toLowerCase());
                $('#slug-modal').val(slug);
                $('#slug').val(slug);
            });

            $('#slug-modal').keyup(function () {
                var slug = convertToKebabCase($(this).val().toLowerCase());
                $('#slug-modal').val(slug);
            });

            $("#save-seo").click(function () {
                var slug = $('#slug-modal').val();
                $('#slug').val(slug);
                $('#change-slug').modal('toggle');
            });
			
			$("#document-popup").click(function () {			
                openPopup();
            });

            $("#remove-document").click(function () {
                $('#added_document').html('');
                $('#remove-document').addClass('invisible')
                $('#fileName').val('');
            });
        });
		
		function openPopup() {
            CKFinder.popup({
                chooseFiles: true,
                onInit: function (finder) {
                    finder.on('files:choose', function (evt) {
                        var file = evt.data.files.first();
                        $('#added_document').html('<a href="' + base_url + file.getUrl() + '">' + base_url + file.getUrl() + "</a>");
                        $('#remove-document').removeClass('invisible');
                        $('#fileName').val(file.getUrl());

                    });
                }
            });
		}
    </script>
@endsection