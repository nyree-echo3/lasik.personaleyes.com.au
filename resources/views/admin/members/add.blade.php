@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/datepicker/datepicker3.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/members') }}"><i class="fa fa-users"></i> {{ $display_name }}</a></li>
                <li class="active">Add New</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add New</h3>
                        </div>

                        <form method="post" class="form-horizontal" action="{{ url('dreamcms/members/store') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="box-body">
                                <h4>Member Details</h4>

                                <div class="form-group{{ ($errors->has('title')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Title *</label>

                                    <div class="col-sm-10">
                                        <select name="title" class="form-control select2" style="width: 100%;">
                                            <option value="Mr"{{ (old('title') == "Mr") ? ' selected="selected"' : '' }}>
                                                Mr
                                            </option>
                                            <option value="Mrs"{{ (old('title') == "Mrs") ? ' selected="selected"' : '' }}>
                                                Mrs
                                            </option>
                                            <option value="Misss"{{ (old('title') == "Miss") ? ' selected="selected"' : '' }}>
                                                Miss
                                            </option>
                                            <option value="Ms"{{ (old('title') == "Ms") ? ' selected="selected"' : '' }}>
                                                Ms
                                            </option>
                                            <option value="Dr"{{ (old('title') == "Dr") ? ' selected="selected"' : '' }}>
                                                Dr
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('firstName')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">First Name *</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" name="firstName" placeholder="First Name"
                                               value="{{ old('firstName') }}">
                                        @if ($errors->has('firstName'))
                                            <small class="help-block">{{ $errors->first('firstName') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('lastName')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Last Name *</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" name="lastName" placeholder="Last Name"
                                               value="{{ old('lastName') }}">
                                        @if ($errors->has('lastName'))
                                            <small class="help-block">{{ $errors->first('lastName') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('occupation')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Occupation</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" name="occupation" placeholder="Occupation"
                                               value="{{ old('occupation') }}">
                                        @if ($errors->has('occupation'))
                                            <small class="help-block">{{ $errors->first('occupation') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('companyName')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Company Name</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" name="companyName" placeholder="Company Name"
                                               value="{{ old('companyName') }}">
                                        @if ($errors->has('companyName'))
                                            <small class="help-block">{{ $errors->first('companyName') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('address1')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Address (Line 1) *</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" name="address1" placeholder="Address (Line 1)"
                                               value="{{ old('address1') }}">
                                        @if ($errors->has('address1'))
                                            <small class="help-block">{{ $errors->first('address1') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('address2')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Address (Line 2)</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" name="address2" placeholder="Address (Line 2)"
                                               value="{{ old('address2') }}">
                                        @if ($errors->has('address2'))
                                            <small class="help-block">{{ $errors->first('address2') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('suburb')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Suburb *</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" name="suburb" placeholder="Suburb"
                                               value="{{ old('suburb') }}">
                                        @if ($errors->has('suburb'))
                                            <small class="help-block">{{ $errors->first('suburb') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ ($errors->has('state')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">State *</label>

                                    <div class="col-sm-10">
                                        <select name="state" class="form-control select2" style="width: 100%;">
                                            <option value="ACT"{{ (old('state') == "ACT") ? ' selected="selected"' : '' }}>
                                                ACT
                                            </option>
                                            <option value="QLD"{{ (old('state') == "QLD") ? ' selected="selected"' : '' }}>
                                                QLD
                                            </option>
                                            <option value="NSW"{{ (old('state') == "NSW") ? ' selected="selected"' : '' }}>
                                                NSW
                                            </option>
                                            <option value="NT"{{ (old('state') == "NT") ? ' selected="selected"' : '' }}>
                                                NT
                                            </option>
                                            <option value="SA"{{ (old('state') == "SA") ? ' selected="selected"' : '' }}>
                                                SA
                                            </option>
                                            <option value="TAS"{{ (old('state') == "TAS") ? ' selected="selected"' : '' }}>
                                                TAS
                                            </option>
                                            <option value="VIC"{{ (old('state') == "VIC") ? ' selected="selected"' : '' }}>
                                                VIC
                                            </option>
                                            <option value="WA"{{ (old('state') == "WA") ? ' selected="selected"' : '' }}>
                                                WA
                                            </option>
                                            <option value="OTHER"{{ (old('state') == "OTHER") ? ' selected="selected"' : '' }}>
                                                OTHER
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('postcode')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Postcode *</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" name="postcode" placeholder="Postcode"
                                               value="{{ old('postcode') }}">
                                        @if ($errors->has('postcode'))
                                            <small class="help-block">{{ $errors->first('postcode') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('phoneMobile')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Mobile</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" name="phoneMobile" placeholder="mobile"
                                               value="{{ old('phoneMobile') }}">
                                        @if ($errors->has('phoneMobile'))
                                            <small class="help-block">{{ $errors->first('phoneMobile') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('phoneLandline')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Phone (Landline)</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" name="phoneLandline" placeholder="Phone (Landline)"
                                               value="{{ old('phoneLandline') }}">
                                        @if ($errors->has('phoneLandline'))
                                            <small class="help-block">{{ $errors->first('phoneLandline') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('email')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Email *</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" name="email" placeholder="email"
                                               value="{{ old('email') }}">
                                        @if ($errors->has('email'))
                                            <small class="help-block">{{ $errors->first('email') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <h4>Membership Details</h4>

                                <div class="form-group{{ ($errors->has('type_id')) ? ' has-error' : '' }}"
                                     id="type_selector">
                                    <label class="col-sm-2 control-label">Member Type *</label>

                                    <div class="col-sm-10{{ ($errors->has('type_id')) ? ' has-error' : '' }}">
                                        @if(count($types)>0)
                                            <select name="type_id" class="form-control select2"
                                                    data-placeholder="All" style="width: 100%;">
                                                @foreach($types as $type)
                                                    <option value="{{ $type->id }}"{{ (old('type_id') == $type->id) ? ' selected="selected"' : '' }}>{{ $type->name }}</option>
                                                @endforeach
                                            </select>
                                        @else
                                            <div class="callout callout-danger">
                                                <h4>No member type found!</h4>
                                                <a href="{{ url('dreamcms/members/add-type') }}">Please click here to
                                                    add member type</a>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ ($errors->has('dateJoin')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Join Date *</label>
                                    <div class="col-sm-10">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input name="dateJoin" type="text"
                                                   class="form-control pull-right datepicker"
                                                   value="{{ old('dateJoin') }}">
                                        </div>
                                        @if ($errors->has('dateJoin'))
                                            <small class="help-block">{{ $errors->first('dateJoin') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ ($errors->has('dateExpire')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Expiry Date *</label>
                                    <div class="col-sm-10">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input name="dateExpire" type="text"
                                                   class="form-control pull-right datepicker"
                                                   value="{{ old('dateExpire') }}">
                                        </div>
                                        @if ($errors->has('dateExpire'))
                                            <small class="help-block">{{ $errors->first('dateExpire') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ ($errors->has('password')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Password *</label>
                                    <div class="col-sm-10">
                                        <input type="password" class="form-control" name="password">
                                        @if ($errors->has('password'))
                                            <small class="help-block">{{ $errors->first('password') }}</small>
                                        @endif
                                    </div>
                                </div>
                                @php
                                    $newsletters = 'active';
                                    if(count($errors)>0){
                                       if(old('newsletters')=='on'){
                                        $newsletters = 'active';
                                       }else{
                                        $newsletters = '';
                                       }
                                    }
                                @endphp
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Newsletters</label>
                                    <div class="col-sm-10">
                                        <label>
                                            <input class="page_status" type="checkbox" data-toggle="toggle"
                                                   data-size="mini"
                                                   name="newsletters" {{ $newsletters == 'active' ? ' checked' : null }}>
                                        </label>
                                    </div>
                                </div>
                                @php
                                    $status = 'active';
                                    if(count($errors)>0){
                                       if(old('live')=='on'){
                                        $status = 'active';
                                       }else{
                                        $status = '';
                                       }
                                    }
                                @endphp
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Status</label>
                                    <div class="col-sm-10">
                                        <label>
                                            <input class="page_status" type="checkbox" data-toggle="toggle"
                                                   data-size="mini"
                                                   name="live" {{ $status == 'active' ? ' checked' : null }}>
                                        </label>
                                    </div>
                                </div>

                            </div>

                            <div class="box-footer">
                                <a href="{{ url('dreamcms/members') }}" class="btn btn-info pull-right"
                                   data-toggle=confirmation data-title="Your changes will be lost! Are you sure?"
                                   data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                                   data-btn-cancel-label="No">Cancel</a>
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save_close">
                                    Save & Close
                                </button>
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save">Save
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/theme/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".select2").select2();

            $('.datepicker').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy'
            });

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });
        });
    </script>
@endsection