@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/iCheck/all.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/popups') }}"><i class="far fa-file-powerpoint"></i> {{ $display_name }}</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit</h3>
                        </div>

                        <form method="post" class="form-horizontal" action="{{ url('dreamcms/popups/update') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" id="id" name="id" value="{{ $popup->id }}">
                            <div class="box-body">
								<div class="form-group{{ ($errors->has('title')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Title *</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="title" name="title"
                                               placeholder="Title" value="{{ old('title',$popup->title) }}">
                                        @if ($errors->has('title'))
                                            <small class="help-block">{{ $errors->first('title') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('form')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Body *</label>

                                    <div class="col-sm-10">
                                        <textarea id="form" name="form" rows="10" cols="80"
                                                  style="height: 500px;">{{ old('form', $popup->form) }}</textarea>
                                        @if ($errors->has('form'))
                                            <small class="help-block">{{ $errors->first('form') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div id="fb-editor" class="box-body"></div>
                                
                                @php
                                    if(count($errors)>0){
                                       if(old('image')!=''){
                                        $image_image = old('image');
                                       }else{
                                        $image_image = '';
                                       }
                                    }else{
                                        $image_image = $popup->image;
                                    }
                                @endphp
                                <div class="form-group {{ ($errors->has('image')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Image</label>
                                    <div class="col-sm-10">
                                        <input type="hidden" id="image" name="image"
                                               value="{{ old('image',$image_image) }}">
                                        <button id="image-popup" type="button" class="btn btn-info btn-sm">Upload
                                            Image
                                        </button>
                                        @php
                                            $class = ' invisible';
                                            if($image_image!=''){
                                                $class = '';
                                            }
                                        @endphp
                                        <button id="remove-image" type="button"
                                                class="btn btn-danger btn-sm{{ $class }}">Remove Image
                                        </button>
                                        <br/><br/>
                                        <span id="added_image">
                                        @if($image_image!='')
                                                <image src="{{ old('image',$image_image) }}"/>
                                            @endif
                                        </span>
                                        @if ($errors->has('image'))
                                            <small class="help-block">{{ $errors->first('image') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>
							
							<!--<div class="form-group">
                                    <label class="col-sm-2 control-label">Popup Position *</label>
                                    <div class="col-sm-10">
                                       <select id="position" name="position">
                                       	   <option value="top-left" {{ ($popup->popup_position == 'top-left' ? ' selected' : '') }}>Top Left</option>
                                       	   <option value="top-right" {{ ($popup->popup_position == 'top-right' ? ' selected' : '') }}>Top Rigth</option>
                                       	   <option value="bottom-left" {{ ($popup->popup_position == 'bottom-left' ? ' selected' : '') }}>Bottom Left</option>
                                       	   <option value="bottom-right" {{ ($popup->popup_position == 'bottom-right' ? ' selected' : '') }}>Bottom Right</option>
                                       </select>
                                    </div>
                                </div>-->
							
                            @php
                                if(count($errors)>0){
                                   if(old('live')=='on'){
                                    $status = 'active';
                                   }else{
                                    $status = '';
                                   }
                                }else{
                                    $status = $popup->status;
                                }
                            @endphp
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Status *</label>
                                <div class="col-sm-10">
                                    <label>
                                        <input class="page_status" type="checkbox" data-toggle="toggle" data-size="mini"
                                               id="live" name="live" {{ $status == 'active' ? ' checked' : '' }}>
                                    </label>
                                </div>
                            </div>

                            <div class="box-footer">
                                <a href="{{ url('dreamcms/popups') }}" id="cancel" type="button" class="btn btn-info pull-right">Cancel</a>
                                <button id="clear" type="button" class="btn btn-info pull-right">Clear</button>
                                <button id="save" type="button" class="btn btn-info pull-right">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
    <script src="{{ asset('/components/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('/components/formBuilder/dist/form-builder.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
			CKEDITOR.replace('form');
            $(".select2").select2();

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });
			
			$("#image-popup").click(function () {
                openPopup();
            });

            $("#remove-image").click(function () {
                $('#added_image').html('');
                $('#remove-image').addClass('invisible')
                $('#image').val('');
            });
			
			
			// Form Builder
			var options = {
                defaultFields : {!! $popup->form !!},
                disableFields : ['autocomplete','button','file','hidden'],
                disabledAttrs: ['access'],
                showActionButtons: false,
            };
            //var formBuilder = $('#fb-editor').formBuilder(options);
			
			$('#save').click(function(){					
                $.ajax({
                    type: "POST",
                    url: '{{ url("dreamcms/popups/update") }}',
                    data:  {
                        'id':$('#id').val(),
						'title':$('#title').val(),
						'live':$('#live').prop('checked'),
						'form':$('#form').val(), //formBuilder.actions.getData('json'),
						'image':$('#image').val(),
						'position':$('#position').val(),
                    },					
                    success: function (response) {							
                        if(response.status=="success"){
                            toastr.options = {"closeButton": true}
                            toastr.success('{{ $display_name }} form saved');
							$(location).attr('href', '{{ url("dreamcms/popups") }}')
                        }

                        if(response.status=="fail"){						
                            toastr.options = {"closeButton": true}
                            toastr.error(response.message);
                        }
                    }
                });
            });
			
			$('#clear').click(function(){
                formBuilder.actions.clearFields();
            });
			
        });
		
		function openPopup() {
            CKFinder.popup({
                chooseFiles: true,
                onInit: function (finder) {
                    finder.on('files:choose', function (evt) {
                        var file = evt.data.files.first();
                        $('#added_image').html('<image src="' + base_url + file.getUrl() + '">');
                        $('#remove-image').removeClass('invisible');
                        $('#image').val(file.getUrl());

                    });
                    finder.on('file:choose:resizedImage', function (evt) {
                        $('#added_image').html('<image src="' + base_url + evt.data.resizedUrl + '">');
                        $('#remove-image').removeClass('invisible');
                        $('#image').val(evt.data.resizedUrl);
                    });
                }
            });
        }
    </script>
@endsection