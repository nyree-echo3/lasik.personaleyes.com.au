<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <!-- Crazyegg -->
    <script type="text/javascript" src="//script.crazyegg.com/pages/scripts/0087/7243.js" async="async"></script>
    
    <title>Online Eye Test</title>
    <link href="{{ asset('/components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/site/online-eye-test.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/components/font-awesome/web-fonts-with-css/css/fontawesome-all.css') }}">
    <link rel="shortcut icon" href="{{ url('') }}/favicon.ico?">
    <link rel="apple-touch-icon" href="{{ url('') }}/apple-icon.png">
    @yield('styles')

</head>
<body>
<header class="blog-header">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <img src="{{ url('') }}/images/site/PE-logo-online-eye-test.png" title="PersonalEYES" alt="PersonalEYES - Online Eye Test"/>
        </div>
    </div>
</header>

<main role="main">
    @yield('content')
</main>

<footer class="blog-footer">
    <p>&copy; 2019 <a ref="nofollow" href="http://www.personaleyes.com.au/">personalEYES Pty Ltd</a>&nbsp;<br />
    Toll Free <a href="tel:1300683937">1300 68 3937</a> [1300 Nu Eyes] | Phone <a href="tel:0288337111">02 8833 7111</a> | Fax 02 8833 7112 | PO Box 301 Parramatta NSW 2150</p>
</footer>

<script src="{{ asset('/components/jquery/dist/jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
        integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
        crossorigin="anonymous"></script>
<script src="{{ asset('/components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $( document ).ready(function() {
        $("#seveninput").keyup(function() {

            if($(this).val().length==8){
                $(".app-tick").show();
            }else{
                $(".app-tick").hide();
            }
        });

        $("#ask-the-doctor").click(function() {
            $("#ask-the-doctor-container").toggle();
        });

        $("#atd-close").click(function() {
            $("#ask-the-doctor-container").hide();
        });
    });

    function closePopup(){
        $("#ask-the-doctor-container").hide();
    }
</script>
@yield('scripts')
@yield('inline-scripts')
</body>
</html>