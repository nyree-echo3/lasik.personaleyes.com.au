@php
    if(isset($mode) && $mode == 'preview')
      $meta_title_inner = "Preview Page";
@endphp

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="keywords" content="{{ (isset($meta_keywords_inner) ? $meta_keywords_inner : $meta_keywords) }}">
    <meta name="description" content="{{ (isset($meta_description_inner) ? $meta_description_inner : $meta_description) }}">
    <meta name="author" content="Echo3 Media">
    <meta name="web_author" content="www.echo3.com.au">
    <meta name="date" content="{{ $live_date }}" scheme="DD-MM-YYYY">
    <meta name="robots" content="all">
    
    <!-- Crazyegg -->
    <script type="text/javascript" src="//script.crazyegg.com/pages/scripts/0087/7243.js" async="async"></script>
    
    <title>{{ (isset($meta_title_inner) ? $meta_title_inner : $meta_title) }}</title>

    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700" rel="stylesheet">
    <link href="{{ asset('/components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/components/font-awesome/css/all.css') }}">
    <link href="{{ asset('/css/site/carousel.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/site/bootstrap-4-navbar.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/jquery-ui/themes/base/all.css') }}">
    <link rel="stylesheet" href="{{ asset('/ckeditor/plugins/mjTab/frontend/mjTab.css') }}">
    <link rel="stylesheet" href="{{ asset('css/site/general.css') }}">
    <link rel="stylesheet" href="{{ asset('css/site/animate.css') }}">
    <link href="{{ asset('/components/css-hamburgers/dist/hamburgers.css') }}" rel="stylesheet">

    <link rel="shortcut icon" href="{{ url('') }}/favicon.ico?">
    <link rel="apple-touch-icon" href="{{ url('') }}/apple-icon.png">

    @yield('styles')

    <!-- Google Analytics -->
    {!! $google_analytics !!}

</head>
<body>
<header>
    @include('site/partials/preview')
    @include('site/partials/navigationV2')
</header>

<main role="main">
    @yield('content')
    @include('site/partials/footer')
    @include('site/partials/scheduling-modal')
</main>

<script src="{{ asset('/components/jquery/dist/jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="{{ asset('/components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/components/jquery-ui/jquery-ui.js') }}"></script>
<script src="{{ asset('/ckeditor/plugins/mjTab/frontend/mjTab.js') }}"></script>
<script src="{{ asset('js/site/bootstrap-4-navbar.js') }}"></script>
<script src="{{ asset('js/site/jquery.textresizer.js') }}"></script>
<script src="{{ asset('js//site/affix.js') }}"></script>
<script src="{{ asset('js/site/button-topofpage.js') }}"></script>
<script src="{{ asset('js/site/number-counter.js') }}"></script>
<script src="{{ asset('js/site/ga-event-tracking.js?v=0.1') }}"></script>

<script type="text/javascript">
	function isScrolledIntoView(element) {
		var scrollBottomPosition = $(window).scrollTop() + $(window).height();
		return ($(element).offset().top < scrollBottomPosition);
	}

	function addClassIfVisible(element) {
		$(element).each(function () {      
			if (isScrolledIntoView(this)) {
			  $(this).addClass('is-visible');
			} else  {
				$(this).removeClass('is-visible');
			}
		});
	}

	addClassIfVisible('.img-doctor');
	addClassIfVisible('.img-doctors');
	addClassIfVisible('.footerContactsWrapperImg');

	$(window).scroll(function () {
		addClassIfVisible('.img-doctor');
		addClassIfVisible('.img-doctors');
		addClassIfVisible('.footerContactsWrapperImg');
	});

    $( document ).ready(function() {
        $('#scheduling-page').modal('handleUpdate');
        $('#online-eye-test').modal('handleUpdate');

        $('.juiAccordion').accordion();
        $('.mj_tab').mjTab();
    });
</script>
   
@yield('scripts')
@yield('inline-scripts')
@yield('inline-scripts-navigation')
@yield('scripts-parallex')
@yield('inline-scripts-parallex')
@yield('inline-scripts-popups')
</body>
</html>