<?php 
   // Set Meta Tags
   $meta_title_inner = ($category_name == "Latest News" ? $category_name : $category_name . " - News"); 
   $meta_keywords_inner = "News"; 
   $meta_description_inner = ($category_name == "Latest News" ? $category_name : $category_name . " - News");  
?>

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

    <div class="blog-masthead">
        <div class="container">

            <div class="row">
                <div class="col-xl-10 col-lg-10 col-md-12 blog-main">                
                            
                    <div class="blog-post"> 
                       
                       <div class="container news-list">
                            <h1>Articles</h1>
                            
							<div class="card-columns">

								    @foreach($items as $item)    
										<div class="card">										
											<div class="card-body">
											<div class="panel-news-item">	
												<a  class="projects-more"  href="{{ url('') }}/news/{{ $item->category->slug }}/{{$item->slug}}">
												    @if ($item->thumbnail != "")											   
														<div class="div-img">
														   <img src="{{ url('') }}/{{ $item->thumbnail }}" alt="{{$item->title}}" />	
														</div>			
													@endif	                                    

													<div class="panel-news-item-title">{{$item->title}}</div>
													<div class="panel-news-item-shortdesc">{!! $item->short_description !!}</div>

													<div class="panel-news-item-readmore">Read More ></div>													                                               
												</a>
												  </div>  
											</div>



										</div>
									@endforeach   
									
							</div><!-- .card-columns -->
						</div><!--  .container news-list -->
                        

					</div><!-- /.blog-post -->
                </div><!-- /.blog-main -->

            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- blog-masthead -->
@endsection

