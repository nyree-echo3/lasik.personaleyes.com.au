@extends('site/layouts/app')

@section('content')

    @include('site/partials/carousel')

    <div class="blog-masthead ">
        <div class="container">

            <div class="row">
                <div class="col-sm-9 blog-main">

                    <div class="blog-post">
                        <h1 class="blog-post-title">Heading 1 lorem ipsum</h1>

                        <p>P Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>

                        <blockquote>
                            <p>Blockquote Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                        </blockquote>

                        <ul>
                            <li>List Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</li>
                            <li>List Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</li>
                        </ul>

                        <hr/>
                        <h2>Heading 2 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</h2>

                        <h3>Heading 3 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</h3>

                        <h4>Heading 4 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</h4>

                        <h5>Heading 5 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</h5>

                        <div class="disclaimer"><p>* As of October 2018</p></div>

                        <h1>Extra Styles</h1>

                        <div class="blue-box">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy
                            nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit
                            amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet
                            dolore magna aliquam erat volutpat.
                        </div>
                        <div class="code-block">
                            <h2>HTML</h2>
                            {{ '<div class="blue-box">Your text goes inside this tag.</div>' }}
                        </div>

                        <div class="blue-outline-box">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
                            nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum
                            dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut
                            laoreet dolore magna aliquam erat volutpat.
                        </div>
                        <div class="code-block">
                            <h2>HTML</h2>
                            {{ '<div class="blue-outline-box">Your text goes inside this tag.</div>' }}
                        </div>

                        <h1>CTA Buttons</h1>

                        <div class="cta-booking">
                            <a href="https://personaleyes-lasik-booking.as.me/schedule.php">Book Free Assessment</a>
                        </div>
                        <div class="code-block">
                            <h2>HTML</h2>
                            {{ '<div class="cta-booking"><a href="https://personaleyes-lasik-booking.as.me/schedule.php">Book Free Assessment</a></div>' }}
                        </div>

                        <div class="cta-download">
                            <a href="https://lasik.personaleyes.com.au/pages/all-about-lasik">Download the Ultimate Guide to LASIK</a>
                        </div>
                        <div class="code-block">
                            <h2>HTML</h2>
                            {{ '<div class="cta-download"><a href="https://lasik.personaleyes.com.au/pages/all-about-lasik">Download the Ultimate Guide to LASIK</a></div>' }}
                        </div>

                        <div class="cta-lasik-guide">
                            <a href="https://lasik.personaleyes.com.au/pages/all-about-lasik/the-ultimate-australian-guide-to-lasik">Download Now</a>
                        </div>
                        <div class="code-block">
                            <h2>HTML</h2>
                            {{ '<div class="cta-lasik-guide"><a href="https://lasik.personaleyes.com.au/pages/all-about-lasik/the-ultimate-australian-guide-to-lasik">Download Now</a></div>' }}
                        </div>

                        <div class="cta-eye-test">
                            <a href="https://lasik.personaleyes.com.au/pages/calculators/australian-online-eye-test">Take The Test</a>
                        </div>
                        <div class="code-block">
                            <h2>HTML</h2>
                            {{ '<div class="cta-eye-test"><a href="https://lasik.personaleyes.com.au/pages/calculators/australian-online-eye-test">Take The Test</a></div>' }}
                        </div>

                        <div class="cta-calculate-save">
                            <a href="https://lasik.personaleyes.com.au/savings-calculator">Calculate Now</a>
                        </div>
                        <div class="code-block">
                            <h2>HTML</h2>
                            {{ '<div class="cta-calculate-save"><a href="https://lasik.personaleyes.com.au/savings-calculator">Calculate Now</a></div>' }}
                        </div>

                        <div class="cta-booking-blue"><a href="https://personaleyes-lasik-booking.as.me/schedule.php">Book Your free appointment</a></div>
                        <div class="code-block">
                            <h2>HTML</h2>
                            {{ '<div class="cta-booking-blue"><a href="https://personaleyes-lasik-booking.as.me/schedule.php">Book Your free appointment</a></div>' }}
                        </div>

                        <div class="cta-booking-blue-header-needs"><a href="https://personaleyes-lasik-booking.as.me/schedule.php">Book an appointment today</a></div>
                        <div class="code-block">
                            <h2>HTML</h2>
                            {{ '<div class="cta-booking-blue-header-needs"><a href="https://personaleyes-lasik-booking.as.me/schedule.php">BOOK AN APPOINTMENT TODAY</a></div>' }}
                        </div>

                    </div><!-- /.blog-post -->
                </div><!-- /.blog-main -->

            </div><!-- /.row -->

        </div><!-- /.container -->
    </div><!-- /.blog-masthead -->

    @include('site/partials/index-news')
    @include('site/partials/index-projects')
    @include('site/partials/index-products')

@endsection
