<?php
// Set Meta Tags
$meta_title_inner = ($category_name == "Latest Properties" ? $category_name : $category_name . " - Properties");
$meta_keywords_inner = "Properties";
$meta_description_inner = ($category_name == "Latest Properties" ? $category_name : $category_name . " - Properties");
?>

@extends('site/layouts/app')

@section('content')

    @include('site/partials/carousel-inner')

    <div class="blog-masthead ">
        <div class="container">

            <div class="row">

                <div class="col-sm-12 blog-main">

                    <div class="card-columns property-cards">

                        @foreach($items as $property)
                            <div class="card">
                                <a href="{{ $property->url }}"><img class="card-img-top" src="{{ $property->images->first()->location }}" alt="{{ $property->title }}"></a>
                                <div class="card-body">
                                    <h5 class="card-title">{{ $property->title }}</h5>
                                    <p class="card-text">{{ strip_tags($property->headline) }}</p>
                                </div>
                                <div class="card-footer">
                                    <div class="row">
                                        <div class="col-5">
                                            {{ $property->bedrooms }} <i class="fas fa-bed mr-1"></i>
                                            {{ $property->bathrooms }} <i class="fas fa-shower mr-1"></i>
                                            {{ $property->garage_spaces }} <i class="fas fa-car"></i>
                                        </div>
                                        @if($property->category->slug=='rent')
                                        <div class="col-7">
                                            ${{ number_format($property->rental_per_week,2) }} PW
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <div id="pagination">{{ $items->links() }}</div>

                </div><!-- /.blog-main -->

            </div><!-- /.row -->

        </div><!-- /.container -->
    </div>
@endsection
