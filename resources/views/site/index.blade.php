@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel')

@include('site/partials/index-home1')
@include('site/partials/index-doctors')
@include('site/partials/index-panel-cta')
@include('site/partials/index-home')

@include('site/partials/index-locations')

@include('site/partials/index-double-panel')

@endsection
