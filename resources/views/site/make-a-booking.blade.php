@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead ">         
    <div class="container">
          <div class="blog-post">            
            <h1 class="blog-post-title" style="text-align:center;">Make A Booking</h1>
            
			<!-- Start of Meetings Embed Script -->
				<div class="meetings-iframe-container" data-src="https://meetings.hubspot.com/marketing422?embed=true"></div>
				<script type="text/javascript" src="https://static.hsappstatic.net/MeetingsEmbed/ex/MeetingsEmbedCode.js"></script>
			<!-- End of Meetings Embed Script -->

           
          </div><!-- /.blog-post -->               
    </div><!-- /.container -->
</div><!-- /.blog-masthead -->
    
@include('site/partials/index-news')
@include('site/partials/index-projects')
@include('site/partials/index-products')

@endsection
