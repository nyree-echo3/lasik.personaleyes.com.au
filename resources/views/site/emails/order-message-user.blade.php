<!DOCTYPE html>
<html>
	<head>
		<style>
			body { font: 14px/18px normal Arial,Helvetica,sans-serif; width: 600px; color: #333333;}
			h1 {color: #000000; font-size:16px; line-height: 120%; font-weight: bold; padding: 15px 0;}
			h2 { color: #004257; font-weight: bold; font-size: 16px; line-height: 110%; padding: 15px 0 5px 0; border-bottom: 1px #e8e8e8 solid}
			h3 {color: #1f8A70; font-weight: bold; font-size: 14px; padding: 0px 0 0 0;}
			h4 {color: #1f8A70; font-weight: normal; font-size: 13px; padding: 0px 0 0 0;}
			.order {width:100%; clear: both; margin-top:30px;}			
			.order-row {width:100%; clear: both;}
			
			.order-row-hdr {width:30%; float:left; font-weight:600;}
			.order-row-txt {width:70%; float:left;}
			
			.address-row {width:48%; border: 1px solid #D0D9DB; margin:10px 0 10px 0px;}
			.address-row-left { float:left;}
			.address-row-right { float:right;}
			.address-row-hdr {width:100%; font-weight:600; background: #F1F3F3; border-bottom: 1px solid #D0D9DB; padding: 10px 0;}
			.address-row-txt {width:100%; padding: 10px 0;}
			.address-row-hdr span {padding: 10px;}
			.address-row-txt span {padding: 10px;}
			
			.items-row {width:100%; border: 1px solid #D0D9DB; margin-bottom: 10px; clear: both;}
			.items-row-hdr {width:100%; font-weight:600; background: #F1F3F3; border-bottom: 1px solid #D0D9DB; padding: 10px 0;}
			.items-row-txt {width:100%; padding: 10px 0;}
			.items-row-hdr span {padding: 10px;}
			.items-row-txt span {padding: 10px;}
			
			table {width:100%; padding: 10px;}
			th {text-align:left;}
			.col-center {text-align: center;}
			.col-right {text-align: right;}
			.sp-success {color: #093;}
			.sp-error {color:#FB0307;}
		</style>	
	</head>
	
	<body>
	   <img src="{{ url('') }}/images/site/email-logo.png">
	   
	   Hi {{ $order->billpayers->firstname }},<br><br>
	   Thank you for shopping with us!  Here is a summary and confirmation of your order with us.<br> 	   

		<div class="order">
		   <div class="order-row">
			  <div class="order-row-hdr">Order Number</div>
			  <div class="order-row-txt">{{ $order->number }}</div>
		   </div>
		   
		   <div class="order-row">
			  <div class="order-row-hdr">Order Date</div>
			  <div class="order-row-txt">{{ date("d/m/Y H:i:s", strtotime($order->created_at)) }}</div>
		   </div>

	       <div class="order-row">
		      <div class="address-row address-row-left">
				  <div class="address-row-hdr"><span>Bill To</span></div>
				  <div class="address-row-txt">
					  <span><strong>{{ $order->billpayers->firstname }} {{ $order->billpayers->lastname }}</strong></span><br>
					  <span>{{ $order->billpayers->addresses->address }}</span><br>
					  @if ($order->billpayers->addresses->address2 != "") <span>{{ $order->billpayers->addresses->address2 }}</span><br>@endif
					  <span>{{ $order->billpayers->addresses->city }} {{ $order->billpayers->addresses->state }} {{ $order->billpayers->addresses->postal_code }}</span><br><br>

					  <span><strong>{{ $order->billpayers->email }}</strong></span>
				  </div>
			  </div>
			  
			  <div class="address-row address-row-right">
				  <div class="address-row-hdr"><span>Ship To</span></div>
				  <div class="address-row-txt">
					  <span><strong>{{ $order->shiptoaddresses->name }}</strong></span><br>
					  <span>{{ $order->shiptoaddresses->address }}</span><br>
					  @if ($order->shiptoaddresses->address2 != "") <span>{{ $order->shiptoaddresses->address2 }}</span><br>@endif
					  <span>{{ $order->shiptoaddresses->city }} {{ $order->shiptoaddresses->state }} {{ $order->shiptoaddresses->postal_code }}</span><br><br>
				  </div>
			  </div>
			</div>		 

			<div class="items-row">
			   <div class="items-row-hdr"><span>Ordered Items</span></div>
			   
			   <div class="items-row-txt">			      
					<table>
						<tr>
							<th>#</th>
							<th>Name</th>							
							<th class='col-center'>Qty</th>
							<th class='col-right'>Price</th>
							<th class='col-right'>Subtotal</th>
						</tr>

						@php
						   $itemCounter = 0;
						   $orderTotal = 0;					           
						@endphp

						@foreach ($order->items as $item) 
							@php
							   $itemCounter++;

							   $subTotal = $item->price * $item->quantity;
							   $orderTotal += $subTotal;
							@endphp
							<tr>
								<td>{{ $itemCounter }}</td>
								<td>{{$item->name}}</td>							
								<td class='col-center'>{{$item->quantity}}</td>
								<td class='col-right'>${{number_format($item->price, 2)}}</td>
								<td class='col-right'>${{number_format($subTotal, 2)}}</td>
							</tr>
						@endforeach

						<tr>
							<th class='col-right' colspan="4">Order Total (includes free shipping)</th>							
							<th class='col-right'>${{number_format($orderTotal, 2)}}</th>
						</tr>
					</table>	
				</div>
			</div>
			
			<div class="order-row">
			  <div class="order-row-hdr">Payment Status</div>
			  <div class="order-row-txt">{{ ucfirst($order->paid_status) }}</div>
		   </div>
		   
		   <div class="order-row">
			  <div class="order-row-hdr">eWAY Transaction</div>
			  <div class="order-row-txt">{{ $order->paid_transaction_number }} <span class="{{ ($order->paid_transaction_result == "Successful" ? "sp-success" : "sp-error") }}">{{ $order->paid_transaction_result }}</span></div>
		   </div>
		   
		   <div class="order-row">
			  <div class="order-row-hdr">Status</div>
			  <div class="order-row-txt">{{ ucfirst($order->status) }}</span></div>
		   </div>
			
		</div>

        <img src="{{ url('') }}/images/site/email-thanks.png">
	</body>
</html>
