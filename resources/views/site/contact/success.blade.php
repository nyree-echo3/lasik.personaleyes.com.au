@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

<div class="blog-masthead ">         
    <div class="container">

      <div class="row">                                   
        <div class="col-xl-10 col-lg-10 col-md-12 blog-main">

          <div class="blog-post">   
               <h1 class="blog-post-title">Contact us</h1>    
               <p>Thank you for your enquiry. We will be in touch with you shortly.</p>             
            </div>
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
@endsection            
