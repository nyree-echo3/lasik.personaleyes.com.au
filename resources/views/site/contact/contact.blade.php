@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/formvalidation/dist/css/formValidation.css') }}">
@endsection

@extends('site/layouts/app')

@section('content')

    @include('site/partials/carousel-inner')

    <div class="blog-masthead ">
        <div class="container">

            <div class="row">
                <div class="col-xl-10 col-lg-10 col-md-12 blog-main">

                    <div class="blog-post">
                        <h1 class="blog-post-title">Contact us</h1>
                        <!--[if lte IE 8]>
                        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
                        <![endif]-->
                        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
                        <script>
                            hbspt.forms.create({
                                portalId: "5064462",
                                formId: "7f6ae947-750c-45b3-8daa-c691e2b3521b"
                            });
                        </script>
                    </div><!-- /.blog-post -->
                </div><!-- /.blog-main -->

            </div><!-- /.row -->

        </div><!-- /.container -->
    </div><!-- /.blog-masthead -->

@endsection
@section('scripts')
    <script src="{{ asset('/components/formBuilder/dist/form-render.min.js') }}"></script>
    <script src="{{ asset('/components/formvalidation/dist/js/formValidation.js') }}"></script>
    <script src="{{ asset('/components/formvalidation/dist/js/plugins/Bootstrap.js') }}"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection
