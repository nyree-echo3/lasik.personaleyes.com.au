@if (isset($allPages) && sizeof($allPages) > 1)    
	<div class="lasik-guide-overview carousel">
	  <div class="carousel-inner">                
		  <div class="carousel-item active">		      
			 <img class="slide" src="{{ $allPages[1]->thumbnail }}" alt="{{ $allPages[1]->title }}">

			 <div class="h-100">
				<div class="row h-100 carousel-caption">
				   <div class="carousel-caption-inner justify-content-center">
					  <div class="carousel-caption-h1">{{ $allPages[1]->title }}</div>
					  <div class="carousel-caption-txt">{!! $allPages[1]->body !!}</div> 		  					  
				   </div>
				</div>
			 </div>
		  </div>

	  </div>
	</div> 
@endif