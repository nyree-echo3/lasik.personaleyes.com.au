<div class="home-intro">
    <div class="container">
        <h1>The personalEYES difference</h1>
        
        <div class="row">
            <div class="col-lg-3 home-intro-img">
                <img src="{{ url('') }}/images/site/icon-blue1.png" alt="Years performing eye surgery in Australia" title="Years performing eye surgery in Australia">
                <div class="home-intro-num"><span class='count' data-count='48'></span></div>			    
                <div class="home-intro-txt">Years performing eye surgery in Australia</div>
            </div>
            
            <div class="col-lg-3 home-intro-img">
                <img src="{{ url('') }}/images/site/icon-blue2.png" alt="Successful procedures performed" title="Successful procedures performed">
                <div class="home-intro-num"><span class='count' data-count='10000'></span></div>	
                <div class="home-intro-txt">Successful procedures performed</div>
            </div>
            
            <div class="col-lg-3 home-intro-img">
                <img src="{{ url('') }}/images/site/icon-blue3.png" alt="Our patients who are ‘completely satisfied’ after treatment." title="Our patients who are ‘completely satisfied’ after treatment.">
                <div class="home-intro-num"><span class='count' data-count='98'></span>%</div>	
                <div class="home-intro-txt">Our patients who are ‘completely satisfied’ after treatment.</div>
            </div>
            
            <div class="col-lg-3 home-intro-img">
                <img src="{{ url('') }}/images/site/icon-blue4.png" alt="Aftercare access to your surgeon" title="Aftercare access to your surgeon">
                <div class="home-intro-num"><span class='count' data-count='24'></span>/<span class='count' data-count='7'></span></div>	
                <div class="home-intro-txt">Aftercare access to your surgeon</div>
            </div>            
        </div>
        
        <div class='home-intro-booking'>   
			<button class="acuity-embed-button btn-booking" data-toggle="modal" data-target="#scheduling-page">Book a Free Assessment Today </button>
		</div>
    </div>
</div>