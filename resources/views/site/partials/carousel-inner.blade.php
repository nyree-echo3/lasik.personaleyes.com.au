@if($header_image)
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner inside-page">
        <div class="carousel-item active">
            
            @if (isset($page) && $page->thumbnail != "")
                <img class="slide-img slide" src="{{ $page->thumbnail }}" alt="Header Image">
               <img class="slide-img-resp slide" src="{{ url('') }}/{{ substr($page->thumbnail, 0, -4) }}-mobile{{ substr($page->thumbnail, -4) }}" alt="Header Image">
            @else
               <img class="slide-img slide" src="{{ $header_image }}" alt="Header Image">
               <img class="slide-img-resp slide" src="{{ url('') }}/{{ substr($header_image, 0, -4) }}-mobile{{ substr($header_image, -4) }}" alt="Header Image">
            @endif
            
            <div class="container">
                <div class="carousel-caption-inside">
                    <div class="carousel-caption-inner">
                        <div class="carousel-caption-h1">{{ $category_name }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif