<div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="10000">
  <ol class="carousel-indicators">
    <?php $counter = 0; ?>
    @foreach($images as $image)        
       <li data-target="#myCarousel" data-slide-to="{{ $counter }}" class="{{ $counter == 0 ? 'active' : '' }}"></li>       
       <?php $counter++;  ?>
    @endforeach 
  </ol>
  <div class="carousel-inner">        
        <?php $counter = 0; ?>
        @foreach($images as $image) 
            <?php $counter++;  ?>
               
			<div class="carousel-item {{ $counter == 1 ? ' active' : '' }}">
		      @if (strtolower(substr($image->location, -3)) == "mp4")
	             <div class="video-container">
					  <video autoplay loop>
						<source src="{{ url('') }}/{{ $image->location }}" type="video/mp4" />
					  </video>
				 </div>
		      @else
			     <img class="slide-img slide" src="{{ url('') }}/{{ $image->location }}" alt="{{ $image->title }}">
			     <img class="slide-img-resp slide" src="{{ url('') }}/{{ substr($image->location, 0, -4) }}-mobile{{ substr($image->location, -4) }}" alt="{{ $image->title }}">
			  @endif
			  
			  <!--<div class="container">-->
			  <div class="h-100">
                 <div class="row align-items-center h-100 carousel-caption">
			        <div class="carousel-caption-inner justify-content-center">
					  @if ( $image->title != "") <div class="carousel-caption-h1">{{ $image->title }}</div> @endif
					  @if ( $image->description != "") <div class="carousel-caption-h2">{{ $image->description }}</div> @endif

					  @if ( $image->url != "" && $image->url2 != "")
						  <div class="carousel-caption-cta">
							 <div class="carousel-caption-cta-dbl">

								 @if($image->urlname == 'Book an Appointment Today')
								 <button class="carousel-caption-cta-1" href="{{ $image->url }}" data-toggle="modal" data-target="#scheduling-page">{{ $image->urlname }}</button>
								 @else
								 <a class="carousel-caption-cta-1" href="{{ $image->url }}" role="button">{{ $image->urlname }}</a>
								 @endif

							 </div>
							 <div class="carousel-caption-cta-dbl">

								 @if($image->urlname == 'Book an Appointment Today')
								 <button class="carousel-caption-cta-2" href="{{ $image->url2 }}" data-toggle="modal" data-target="#info-pack">{{ $image->urlname2 }}</button>
								 @else
								 <a class="carousel-caption-cta-2" href="{{ $image->url2 }}" role="button">{{ $image->urlname2 }}</a>
								 @endif

							 </div>				        
						  </div> 
					  @elseif ( $image->url != "")
						  <div class="carousel-caption-cta">			            
							   <a class="carousel-caption-cta-1" href="{{ $image->url }}" role="button">{{ $image->urlname }}</a>				        
						  </div> 
					  @endif
				  </div>		
	  </div>
				
			  </div>
			</div>
        @endforeach
   
  </div>
  <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>