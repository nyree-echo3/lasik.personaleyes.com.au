<div class="home-askdoctor">
   <div class="container">
      <img src= "{{ url('') }}/images/site/icon-ask.png" alt="Icon Ask A Doctor" title="Icon Ask A Doctor">
      <h2>Ask a Doctor</h2>
      
      <div class="row">        
         <div class="col-lg-12">
            
         
            <p>Do you have a question about your vision and would like it answered promptly by one of our doctors?</p> 
            
            <div class="btn-askdoctor"><a data-toggle="modal" href="#modalAskTheDoctor">Ask A Question</a></div>             
		 </div>
                 
      </div>
   </div>
</div>

<!-- The Modal -->
<div class="modal fade modal-AskTheDoctor" id="modalAskTheDoctor">
  <div class="modal-dialog modal-lg modal-dialog-centered">
	<div class="modal-content">

	  <!-- Modal Header -->
	  <div class="modal-header">
		<div class="modal-header-content">
	       <div class="modal-header-content-header">
		      <div><img src= "{{ url('') }}/images/site/icon-ask.png" alt="Icon Ask A Doctor" title="Icon Ask A Doctor"></div>	
		      <div class="modal-header-content-header-padding">Ask a Doctor</div>		   
		   </div>
		</div>
		<hr>
		<div class="modal-btn">
		   <a class="btnMore" href='#' data-dismiss="modal"><i id="btnMoreDown" class="fas fa-times"></i></a>
		</div>											
	  </div>

	  <!-- Modal body -->
	  <div class="modal-body">
		<div class="modal-body-body">
	 
		   <!-- HubSpot Form - START -->
            <!--[if lte IE 8]>
			<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
			<![endif]-->
			<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
			<script>
			  hbspt.forms.create({
				portalId: "5064462",
				formId: "03047f88-85d6-45c6-b78a-26492b8988c5"
			});
			</script>
            <!-- HubSpot Form - END -->		
            
            								   								   
		</div>
	  </div>

	  <!-- Modal footer -->
	  <div class="modal-footer">										
	  </div>

	</div>
  </div>
</div> 							