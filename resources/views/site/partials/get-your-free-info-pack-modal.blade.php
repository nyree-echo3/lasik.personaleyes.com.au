<div class="modal fade" id="info-pack" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-5">
						<img src="{{ url('') }}/images/site/logo.png" title="{{ $company_name }}" alt="{{ $company_name }}" class="scheduling-page-logo">
					</div>
					<div class="col-sm-7 d-none d-lg-block">
						<h1 class="scheduling-page-header">PersonalEYES</h1>
					</div>
				</div>
				<!--[if lte IE 8]>
				<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
				<![endif]-->
				<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
				<script>
                    hbspt.forms.create({
                        portalId: "5064462",
                        formId: "2072b349-b1f1-438f-8a6e-3b1284ed3c22"
                    });
				</script>
			</div>
		</div>
	</div>
</div>