<div class='navbar-booking-mobile'>   
   <a href="https://personaleyes-lasik-booking.as.me/schedule.php" target="_blank" class="acuity-embed-button btn-booking-mobile" >Book Now</a>        
    
   <a class="contact-icon" href="{{ url('contact') }}"><i class="fas fa-envelope"></i></a>
   <a id="phone-number-mobile" class="phone-icon" href='tel:{{ str_replace(' ', '', $phone_number) }}' class="btn-booking"><i class="fas fa-phone-alt"></i></a>
</div>