<div class="modal fade" id="online-eye-test" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<iframe id="oet-iframe" src="{{ url('online-eye-test/step1') }}" width="100%" height="580" frameBorder="0"></iframe>
			</div>
		</div>
	</div>
</div>