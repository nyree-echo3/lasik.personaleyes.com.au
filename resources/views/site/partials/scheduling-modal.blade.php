<div class="modal fade" id="scheduling-page" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-5">
						<img src="{{ url('') }}/images/site/logo.png" title="{{ $company_name }}" alt="{{ $company_name }}" class="scheduling-page-logo">
					</div>
					<div class="col-sm-7 d-none d-lg-block">
						<h1 class="scheduling-page-header">Online Booking</h1>
					</div>
				</div>
				<iframe src="https://personaleyes-lasik-booking.as.me/schedule.php" width="100%" height="650" frameBorder="0"></iframe><script src="https://embed.acuityscheduling.com/js/embed.js" type="text/javascript"></script>
			</div>
		</div>
	</div>
</div>