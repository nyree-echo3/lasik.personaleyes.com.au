@section('styles')
    <link rel="stylesheet" href="{{ asset('css/site/popup.css') }}">
@endsection
   
<!-- Modal - Popup -->

@php
   switch ($page->popup_position)  {
      case "top-left":
      case "bottom-left": 
         $slideout = "left";
         break;
         
      default:
         $slideout = "right";
         break;   
   }
@endphp

<div class="modal fade modal-{{ $page->popup_position }}" id="modal-popup" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="false" data-focus="false">
  <div class="modal-dialog modal-dialog-slideout modal-dialog-slideout-{{ $slideout }} modal-md" role="document">
    <div class="modal-content">     
      <div class="modal-body">
        <div class="modal-btn">
		    <a class="" href='#' data-dismiss="modal"><i id="btnMoreDown" class="fas fa-times"></i></a>
		 </div>	
       
        {!! $popup->form !!}
      </div>      
    </div>
  </div>
</div>


@section('inline-scripts-popups')
    <script type="text/javascript">
        $(document).ready(function () {
			startTime = {{ $page->popup_start }} * 60000; // Convert minutes to milliseconds
			endTime = {{ $page->popup_end }} * 60000; // Convert minutes to milliseconds			
			
			setTimeout(function(){			  
               $('#modal-popup').modal('show');
            }, startTime);  
			
			setTimeout(function(){			
               $('#modal-popup').modal('hide');
            }, endTime);
        });
    </script>
@endsection	