<div class='navbar-contacts'>
   <a class="contact-icon" href="{{ url('contact') }}"><i class="fas fa-envelope"></i></a>
   <a id="phone-number" href='tel:{{ str_replace(' ', '', $phone_number) }}'><i class="fas fa-phone-alt"></i> {{ $phone_number }}</a>
</div>