<div class="home-intro1">
    <div class="container">
        <div class="row">
            <div class=" col-lg-3 img-doctor">
                <img src="{{ url('') }}/images/site/dr-bala.png" alt="A/Prof Chandra Bala" title="A/Prof Chandra Bala">
            </div>

            <div class="col-lg-6">
                <?php echo $home_intro_text; ?>
                
                 <div class='home-intro-booking'>   
			        <button class="acuity-embed-button btn-booking" data-toggle="modal" data-target="#scheduling-page">Book a Free Assessment Today </button>
		         </div>
            </div>

            <div class="col-lg-3 img-doctor img-doctor-right">
                <img src="{{ url('') }}/images/site/dr-meades.png" alt="Dr Kerrie Meades" title="Dr Kerrie Meades">
            </div>

            <div class="col-lg-3 img-doctors">
                <div class="img-doctor1">
                    <img src="{{ url('') }}/images/site/dr-bala.png" alt="A/Prof Chandra Bala" title="A/Prof Chandra Bala">
                </div>
                <div class="img-doctor2">
                    <img src="{{ url('') }}/images/site/dr-meades.png" alt="Dr Kerrie Meades" title="Dr Kerrie Meades">
                </div>
            </div>
        </div>                                                
    </div>
</div>

<div class="home-doctors-mobile">
   <img src="{{ url('') }}/images/site/drs-bala-meades-mobile.png" alt="A/Prof Chandra Bala & Dr Kerrie Meades" title="A/Prof Chandra Bala & Dr Kerrie Meades">
</div> 