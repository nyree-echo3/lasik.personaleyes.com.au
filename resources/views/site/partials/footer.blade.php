<footer class='footer'>
 <div class='footerContactsWrapper'>
     
     
	 <div class='footerContacts'>
        <div class="panelNav">
	    <div class="container-fluid">
		<div class="row">
       
           <div class="col-lg-4">	
             <div class="footer-logo">
				  <a href="{{ url('') }}" title="{{ $company_name }}"><img src="{{ url('') }}/images/site/logo-bottom.png" title="{{ $company_name }}" alt="{{ $company_name }}"></a>			  

				  <div id="footer-social">	
					<h2>Follow Us</h2>						
					@if ( $social_facebook != "") <a href="{{ $social_facebook }}" target="_blank"><i class='fab fa-facebook-f'></i></a> @endif
					@if ( $social_instagram != "") <a href="{{ $social_instagram }}" target="_blank"><i class='fab fa-instagram'></i></a> @endif
					@if ( $social_twitter != "") <a href="{{ $social_twitter }}" target="_blank"><i class='fab fa-twitter'></i></a> @endif 
					@if ( $social_youtube != "") <a href="{{ $social_youtube }}" target="_blank"><i class='fab fa-youtube'></i></a> @endif
					@if ( $social_linkedin != "") <a href="{{ $social_linkedin }}" target="_blank"><i class='fab fa-linkedin-in'></i></a> @endif
					@if ( $social_googleplus != "") <a href="{{ $social_googleplus }}" target="_blank"><i class='fab fa-google-plus-g'></i></a> @endif	
					@if ( $social_pinterest != "") <a href="{{ $social_pinterest }}" target="_blank"><i class='fab fa-pinterest-p'></i></a> @endif				
				  </div>
				  
				  <div class="google-review">
					 <a href="https://www.google.com.au/search?source=hp&ei=5gcxXYyHI9fB9QPepo-YDQ&q=personaleyes&oq=personaleyes&gs_l=psy-ab.3..0l8j0i10j0.981.5143..5221...3.0..0.194.1856.0j12......0....1..gws-wiz.....0..0i131.JBQ53A4pF5Y&ved=2ahUKEwjH86q51r_jAhUEOSsKHXzbAykQvS4wAHoECAcQLA&uact=5&npsic=0&rflfq=1&rlha=0&rllag=-33799595,151105731,11802&tbm=lcl&rldimm=12210723549260763504&rldoc=1&tbs=lrf:!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:2#rlfi=hd:;si:650661978133997922;mv:!1m2!1d-33.098168367811844!2d151.26419858984377!2m2!1d-34.716511351066714!2d149.16580991796877!4m2!1d-33.91118087282168!2d150.21500425390627!5i9" target="_blank"><img src="{{ url('') }}/images/site/Google-Reviews.png" title="Google Reviews" alt="Google Reviews"></a>
				  </div>			  			  
			   </div>  
           </div>	          	              
                	              
		   @if(count($navigation))	
			   <div class="col-lg-3">
			   <div class="center-block">

			   @php
			   $counter = 0;
			   @endphp

			   @foreach($navigation as $nav_key => $nav_item) 
				  @if ($nav_item->href == "pages/why-us" || $nav_item->href == "locations")						  				  	  				  	  	                		                	                		                
					  </div></div>				                

					  <div class="col-lg-2">
					  <div class="center-block">	

					  @php
					  $counter = 0;
					  @endphp			        				        								  								  
				  @endif					        					       

				  <div class="footer-txt footer-menu-top {{ ($counter > 0 ? "footer-menu-top-gap" : "") }}">
					<a href="{{ url('') }}/{{ $nav_item->href }}">{{ $nav_item->text }}</a>
				  </div>       

				  @if (isset($nav_item->children))
					 @foreach($nav_item->children as $sub_key => $sub_item) 	
						<div class="footer-txt footer-menu">										  					    
							<a href="{{ url('') }}/{{ $sub_item->href }}">{{ $sub_item->text }}</a>																			
						</div>
					 @endforeach
				  @endif   									


				  @php
				  $counter++;
				  @endphp													
			   @endforeach
			   </div></div>  
		   @endif			                  
		   		   
		</div>
	 </div> 
 </div>
 </div>
 
<!--<div class="footerContactsWrapperImg"></div></div>-->
   
 <div class='footerContainer'>  
  <div id="footer-txt-bot"> 
	@if ( $company_name != "")<a href="https://www.personaleyes.com.au/" target="_blank">&copy; {{ date('Y') }} {{ $company_name }} </a> | @endif 
    <a href="{{ url('') }}/pages/other/privacy-policy">Privacy</a> |     
    <a href="{{ url('') }}/pages/other/terms--conditions">Terms</a> |     
    <a href="{{ url('') }}/contact">Contact</a> |     
    <a href="https://www.echo3.com.au" target="_blank">Website by Echo3</a> 
    
  </div>
  </div>
</footer>