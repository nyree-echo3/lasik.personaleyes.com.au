 <div class="home-locations">
	 <div class="container">
      <img src= "{{ url('') }}/images/site/icon-checkpoint.png" alt="Icon Find" title="Icon Find">
      <h2>Find a clinic near you</h2>    
      
      <div class="row">         
         <div class="col-lg-12">
             
            
            @if (isset($home_locations)) 
                @php
				   $state = "";
				   $prevLocation = "";
				@endphp	
				
				@foreach ($home_locations as $item)	
				    @if ($prevLocation != "" && $state == $item->state)
				        &nbsp;|&nbsp;
				    @endif
				   
					@if ($state != $item->state)	
			           @if ($state != "")
		                   </div>
			           @endif
			           
				       <div class="home-locations-div">
					   <div class="home-locations-state">{{$item->state}}</div>					   

					   @php
						  $state = $item->state;
					   @endphp	
					@endif
					
					<div class="home-locations-name"><a class="" href="{{ url('') }}/locations/{{$item->slug }}">{{$item->name }}</a></div>    	
					
					@php
						  $prevLocation = $item->id;
					   @endphp				   				   
				@endforeach
		        </div>  
			@endif		                                                       
						                                                
		 </div>
                 
      </div>
   </div>
   
   <!--<div class="home-locations-img">
       <img src= "{{ url('') }}/images/site/clinic.jpg" alt="Icon Find" title="Icon Find">
   </div>-->
</div>

<div id="parallax" class="parallaxParent parallax-container">   
	<div class="parallax-img">
		<div class="parallax-img-img"></div>
	</div>
</div>										
						
							
@section('scripts-parallex')
<script type="text/javascript" src="{{ asset('/js/site/parallex/TweenMax.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/site/parallex/ScrollMagic.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/site/parallex/animation.gsap.js') }}"></script>
@endsection
										
@section('inline-scripts-parallex') 
	<script>
		// init controller
		var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});

		// build scenes				
		new ScrollMagic.Scene({triggerElement: "#parallax"})
						.setTween("#parallax > div", {y: "80%", ease: Linear.easeNone})										
						.addTo(controller);
        //$(".parallax3-img").addClass('parallax3-img-mod');
		
	</script>
@endsection