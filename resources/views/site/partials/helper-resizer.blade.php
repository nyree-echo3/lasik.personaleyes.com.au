<div class='navbar-resizer'>
   <div class="fontsizer-txt">Text size</div>
   <div class="fontsizer"></div>
</div>

@section('inline-scripts')
<!-- Accessibility - Text Resizer -->     
<script type="text/javascript">
	$(document).ready(function(){		
		$('.fontsizer').jfontsizer({
			applyTo: '.home-intro p, .blog-post p',
			changesmall: '2',
			changelarge: '3',
			expire: 30
		});					
	});
</script>
@endsection
