<div class='navbar-members'>
   @guest("member")
      <a href='{{ url('') }}/register'>Register</a> | 
      <a href='{{ url('') }}/login'>Login</a>  
   @endguest
   
   @auth("member")
      <a href='{{ url('') }}/members-portal'>Members Portal</a> | 
      <a href='{{ url('') }}/logout'>Logout</a>  
   @endauth
</div>