@if (isset($category) && sizeof($category) > 0 && $category[0]->header != "")
    <div id="section"></div>
    
	<div id="myCarousel" class=" lasik-guide-carousel carousel">
	  <div class="carousel-inner">                
		  <div class="carousel-item active">		      
			 <img class="slide-img slide" src="{{ $category[0]->header }}" alt="{{ $category[0]->category }}">
             <img class="slide-img-resp slide" src="{{ url('') }}/{{ substr($category[0]->header, 0, -4) }}-mobile{{ substr($category[0]->header, -4) }}" alt="{{ $category[0]->category }}">
             
			 <div class="h-100">
				<div class="row h-100 carousel-caption">
				   <div class="carousel-caption-inner justify-content-center">
					  <div class="carousel-caption-h1">{{ $allPages[0]->title }}</div>
					  <div class="carousel-caption-txt">{!! $allPages[0]->body !!}</div>
					  <div class="carousel-caption-txt"><div class="btn-download"><a data-toggle="modal" href="#modalDownload">Too busy right now?<br>Download the guide here</a></div>  </div>		  					  
					  <div id="section{{ $allPages[1]->id }}"></div>
				   </div>
				</div>
			 </div>
		  </div>

	  </div>	  	  
	</div> 
@endif