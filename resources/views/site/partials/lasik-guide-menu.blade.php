@if (isset($allPages))
	<div class="lasik-guide-menu">
		<ul>
		@php 
		   $pageCounter = 0;
		   $divCounter = 0;
		@endphp

		@foreach ($allPages as $page)  
		   @php 
			  $pageCounter++;  

			  $page_title = $page->title;
			  switch ($pageCounter)  {
				 case 2: $page_title = "Overview"; break;
				 case 5: $page_title = "LASIK FAQs"; break;
				 case 6: $page_title = "Side-Effects /  Complications"; break;
				 case 8: $page_title = "Surgery Day & Recovery"; break;
				 case 9: $page_title = "Selecting a LASIK Surgeon & Clinic"; break;

			  }                                
		   @endphp

		   @if ($pageCounter != 1 && $pageCounter != 3 )             
			  @php 
				 $divCounter++;  
			   @endphp

			  <li><a id="link{{ $page->id }}" href='#section{{ $page->id }}' onclick="link_onclick({{ $page->id }}, {{ $divCounter }})">{{ $page_title }}</a></li>  
		   @endif
		@endforeach
		</ul>

		<div class="btn-download"><a data-toggle="modal" href="#modalDownload">Download the Ultimate Guide to LASIK here</a></div>

		@include('site/partials/helper-sharing')
	</div>
@endif