<div class="home-panel-cta">
   <div class="container">
	  <div class="row">         		   	 
		  <div class="col-lg-4 home-panel-cta-box">	            
		       <div class="home-panel-cta-box">
		           <div class="home-panel-cta-box-img">
				      <img src="{{ url('') }}/images/site/icon1.png" alt="The Ultimate Australian Guide to LASIK" />			   
				   </div>
				   <div class="home-panel-cta-box-txt">					  
					   <h2>The Ultimate Australian Guide to LASIK</h2>					   
                   </div>
                   <div class="btn-cta btn-cta-mobile"><a href="{{ url('') }}/pages/all-about-lasik" >Download Now</a></div>               			   			   							   
               </div>			  
		  </div><!-- /.col-lg-4 -->				
		
		   <div class="col-lg-4 home-panel-cta-box">
	           <div class="home-panel-cta-box">
	               <div class="home-panel-cta-box-img">
				      <img src="{{ url('') }}/images/site/icon2.png" alt="Take the Australian Online Eye Test" />
				   </div>
				   <div class="home-panel-cta-box-txt">					   
					   <h2>Take the Australian Online Eye Test</h2>					    
				   </div>
                   <div class="btn-cta btn-cta-mobile"><a href="{{ url('pages/calculators/australian-online-eye-test') }}" >Take The Test</a></div>
               </div>             			   			   							   
		  </div><!-- /.col-lg-4 -->				  
		
		  <div class="col-lg-4 home-panel-cta-box">
	           <div class="home-panel-cta-box">
	               <div class="home-panel-cta-box-img">
				      <img src="{{ url('') }}/images/site/icon3.png" alt="Calculate how much you could save with LASIK" />			
				   </div>
				   <div class="home-panel-cta-box-txt">					   
					   <h2>Calculate how much you could save with LASIK</h2>					      			   			   				   
                   </div>
                   <div class="btn-cta btn-cta-mobile"><a href="{{ url('savings-calculator') }}" >Calculate Now</a></div>
               </div>
		  </div><!-- /.col-lg-4 -->
		  
		  <div class="col-lg-4 home-panel-cta-box">
	  	     <div class="home-panel-cta-box">
		  	    <div class="btn-cta btn-cta-full"><a href="{{ url('') }}/pages/all-about-lasik" >Download Now</a></div> 
			 </div>
		  </div>
		  
		  <div class="col-lg-4 home-panel-cta-box">
	  	     <div class="home-panel-cta-box">
		  	    <div class="btn-cta btn-cta-full"><a href="{{ url('pages/calculators/australian-online-eye-test') }}" >Take The Test</a></div>
			 </div>
		  </div>
		  
		  <div class="col-lg-4 home-panel-cta-box">
	         <div class="home-panel-cta-box">
		        <div class="btn-cta btn-cta-full"><a href="{{ url('savings-calculator') }}" >Calculate Now</a></div>
			 </div>
		  </div>			
		  				
	   </div>
   </div>
</div>
