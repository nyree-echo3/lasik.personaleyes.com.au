@extends('site/layouts/online-eye-test')

@section('content')
    <div class="container-fluid" id="cont">
        <div class="row justify-content-center">
            <div class="col-12 test-container email-result">
                <!--[if lte IE 8]>
                <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
                <![endif]-->
                <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
                <script>
                    hbspt.forms.create({
                        portalId: "5064462",
                        formId: "9689edf8-c536-4b60-bfe0-1d11a8d7180d",
                        onFormSubmit: function ($form) {

                            var formData = $form.serialize();
                            $.ajax({
                                type: 'POST',
                                url: '{{ url('online-eye-test/send-result') }}',
                                data: formData
                            });
                        }
                    });
                </script>
                <p>&nbsp;</p>
                <a href="{{ url('online-eye-test/feedback') }}">Click here to send us your feedback</a>
            </div>
        </div>
    </div>
@endsection

@section('inline-scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            $('#oet-iframe', window.parent.document).height('700px');
        });
    </script>
@endsection