@extends('site/layouts/online-eye-test')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="progress-circles">
                <img src="{{ asset('images/site/progress_dot1.png') }}" alt="logo"/>
                <img src="{{ asset('images/site/progress_dot1.png') }}" alt="logo"/>
                <img src="{{ asset('images/site/progress_dot1.png') }}" alt="logo"/>
                <img src="{{ asset('images/site/progress_dot2.png') }}" alt="logo"/>
                <img src="{{ asset('images/site/progress_dot2.png') }}" alt="logo"/>
                <img src="{{ asset('images/site/progress_dot2.png') }}" alt="logo"/>
            </div>
        </div>
        <div class="row justify-content-center">
            <form action="{{ url('online-eye-test/step4') }}" method="post" name="step3form" id="step3form">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="col-12 test-container">
                    <input type="hidden" name="chars" value="{{ $random_string }}">
                    <p><strong id="step3text">{{ $random_string }}</strong></p>
                    <input name="step3input" id="step3input" type="text" class="text-box" value="" />
                    <div class="app-tick"><img src="{{ asset('images/site/tick-small.png') }}" alt="tick-small"/></div>
                    <p>Letters are not case sensitive</p>
                    <input name="next" id="next" type="button" class="button" value="Next" />
                </div>
            </form>
        </div>
        @include('site/online-eye-test/ask-the-doctor')
    </div>
@endsection

@section('inline-scripts')
    <script type="text/javascript">
        $( document ).ready(function() {

            $("#step3input").keyup(function() {
                if($(this).val().length==8){
                    $(".app-tick").show();
                }else{
                    $(".app-tick").hide();
                }
            });

            $("#next").click(function() {
                var step3input = $('#step3input').val();
                if(step3input.length<8){
                    alert ("You've missed some characters! ");
                }else if(step3input.length>8){
                    alert ("You've typed more characters than required !");
                }else{
                    $('#step3form').submit();
                }
            });
        });
    </script>
@endsection
