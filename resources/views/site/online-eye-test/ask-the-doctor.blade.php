@if(Session::get('country')=='Australia')
<div class="row justify-content-center ask-the-doctor-band">
    <div class="atd-text-container">Do you
        have a question about your vision and would like it answered promptly by one of our
        specialised doctors?
        <div>
            <input id="ask-the-doctor" type="button" class="button button-blue" value="Ask The Doctor">
        </div>
    </div>
    <div class="atd-image-container d-none d-sm-block">
        <img src="{{ asset('images/site/doctor_img.png') }}" alt="Ask the Doctor">
    </div>
    <div id="ask-the-doctor-container">
        <div id="atd-close">
            <i class="fas fa-times"></i>
            Close
        </div>
        <p>Do you have a question about your vision and would like it answered promptly by one of our specialised doctors?</p>
        <!--[if lte IE 8]>
        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
        <![endif]-->
        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
        <script>
            hbspt.forms.create({
                portalId: "5064462",
                formId: "b8953ae9-514f-401b-8ead-8f615cb2ccaa",
                onFormSubmit: function ($form) {
                    //parent.closePopup();
                }
            });
        </script>
    </div>
</div>
@endif