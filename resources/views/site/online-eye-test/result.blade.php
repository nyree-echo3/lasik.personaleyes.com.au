@extends('site/layouts/online-eye-test')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 test-container">

                <img src="{{ url('images/site/tick-big.png') }}" alt="tick-big" class="tick-big" />
                <h2 class="cong-test">Congratulations on completing the test!</h2>

                <div class="row">
                    <div class="col-6 but-left">
                        <a href="{{ url('online-eye-test/email') }}" class="result-button">Email test results</a>
                    </div>
                    <div class="col-6 but-right">
                        <a href="{{ url('online-eye-test/step1') }}" class="result-button">Re-do Test</a>
                    </div>
                </div>
            </div>
        </div>
        @include('site/online-eye-test/ask-the-doctor')
    </div>
@endsection

@section('inline-scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            $('#oet-iframe', window.parent.document).height('615px');
        });
    </script>
@endsection
