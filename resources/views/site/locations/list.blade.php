<?php 
   // Set Meta Tags
   $meta_title_inner = "Locations";
   $meta_keywords_inner = "locations";
   $meta_description_inner = "Locations";  

?>

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

    <div class="blog-masthead">
        <div class="container">

            <div class="row">
                <div class="col-xl-10 col-lg-10 col-md-12 blog-main">                
                            
                    <div class="blog-post"> 
                       
                       <div class="container">
                            <div class="row">
							    <div class="col-lg-12"><h1>Locations</h1></div>                            								  

								<div class="col-lg-8">  
							        <div class="locations-contact">                                                                								    
										<div class="locations-contact-txt">
											 <div>
											 <h2>Find out if LASIK is right for you.</h2>
											 <p>We'd love to sit down with you, do a full FREE assessment of your eyesight and lifestyle, answer your burning questions on LASIK and create your very own roadmap to 20/20 vision.</p>
											 </div>
											 <button class="acuity-embed-button btn-booking" data-toggle="modal" data-target="#scheduling-page">Book Free Assessment</button>
										</div>

										<div class="locations-contact-txt">
											 <h2>Contact us today</h2>
											 <p>We'd love you to get in touch with us so we can answer you back promptly.</p>
											 <a href='{{ url('') }}/contact' class="btn-booking">Contact Us</a>
										</div>
									</div>
								 </div>
								 
								 <div class="col-lg-4">         
							       <div class="locations-contact">                      
								      <iframe src='https://www.google.com.au/maps/d/embed?mid=1wEf0gouvWRyV7FfAUhtmLZ70ZRK7ARo7' style="width:100%; height:100%;"></iframe>
								   </div>
								</div>


										@php
										   $state = "";
										@endphp	

										@foreach ($items as $item)	
											@if ($state != $item->state)								           
												<!-- <div class="col-lg-12"><h2>{{$item->state}}</h2></div> -->			

												@php
												  $state = $item->state;
												@endphp	
											@endif

											<div class="col-lg-4 locations-col">
												<div class="location-div">
												   <div class="locations-state">{{$item->state}}</div>
												   <div class="locations-name">{{$item->name}}</div> 

												   <div class="locations-address">
													  {!! $item->address !!}
													  @if ($item->address2 != "") 
														<br>{{$item->address2}} 
													  @endif
													  {{$item->suburb}} {{$item->state}} {{$item->postcode}}<br>

													  @if ($item->phone != "") 
														 <div class="locations-link"><a href='tel:{{ str_replace(")", "", str_replace("(", "", str_replace(" ", "", $item->phone))) }}'><i class="fa fa-phone"></i> {{$item->phone}}</a></div>
													  @endif

													  @if ($item->website != "") 
														 <div class="locations-link"><a href='{{ $item->website }}' target='_blank'><i class="fas fa-map-marker-alt"></i> Find Us</a></div>	 
													  @endif

													  <div class="locations-link"><a class="" href="{{ url('') }}/locations/{{$item->slug }}">More Information <i class='fa fa-chevron-right'></i></a></div>

													  @if ($item->map)<!--<div class="locations-map">{!! $item->map !!}</div>-->@endif

												   </div> 
												</div>
											 </div>

										@endforeach   						
										
						</div><!-- container -->
					</div><!-- /.blog-post -->
                </div><!-- /.blog-main -->

            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- blog-masthead -->
@endsection