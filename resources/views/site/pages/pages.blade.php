<?php
// Set Meta Tags
$meta_title_inner = $page->meta_title;
$meta_keywords_inner = $page->meta_keywords;
$meta_description_inner = $page->meta_description;
?>

@extends('site/layouts/app')

@if($page->slug=='savings-calculator')
@section('styles')
    <link rel="stylesheet" href="{{ asset('css/site/calculator-tab.css') }}">
    <link rel="stylesheet" href="{{ asset('components/rangeslider.js/dist/rangeslider.css') }}">
@endsection
@endif

@section('content')

    @include('site/partials/carousel-inner')


    <div class="blog-masthead ">
        <div class="container">

            <div class="row">
                <div class="col-xl-10 col-lg-10 col-md-12 blog-main">

                    <div class="blog-post">
                        @if (isset($page))
                            <h1 class="blog-post-title">{{$page->title}}</h1>
                            {!! $page->body !!}
                        @endif
                    </div><!-- /.blog-post -->
                    @if($page->slug=='savings-calculator')
                        @include('site/savings-calculator/calculator')
                    @endif
                </div><!-- /.blog-main -->

            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.blog-masthead -->
    @include('site/partials/online-eye-test-modal') 
    
    @if (isset($page) && $page->popup_type != "")    
        @include('site/partials/popup') 
    @endif
@endsection

@if($page->slug=='savings-calculator')
@section('scripts')
    <script src="{{ asset('components/rangeslider.js/dist/rangeslider.js') }}"></script>
    <script src="{{ asset('js/site/savings-calculator.js') }}"></script>
@endsection
@endif



@section('inline-scripts')
    <script type="text/javascript">
        $('#online-eye-test').modal('handleUpdate');
    </script>
@endsection
