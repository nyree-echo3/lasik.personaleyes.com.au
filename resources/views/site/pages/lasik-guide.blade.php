<?php
// Set Meta Tags
if (isset($allPages) && sizeof($allPages) > 0)  {
   $meta_title_inner = $allPages[0]->meta_title;
   $meta_keywords_inner = $allPages[0]->meta_keywords;
   $meta_description_inner = $allPages[0]->meta_description;
}
?>

@extends('site/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/site/lasik-guide.css') }}">
    <link rel="stylesheet" href="{{ asset('css/site/popup.css') }}">
@endsection

@include('site/partials/lasik-guide-carousel')
@include('site/partials/lasik-guide-section1')
 
@section('content')


@if (isset($allPages))
	@php 
	   $pageCounter = 0;
	@endphp

	@foreach ($allPages as $page)   
	   @php 
		  $pageCounter++;                                 
	   @endphp

	   @if ($pageCounter > 2)   
            <div class="blog-masthead lasik-guide">	    
            
               <!-- Header -->
	           <div id="section{{ $page->id }}" class="page-header{{ $pageCounter }}">	
				   <div class="page-header-background page-header-background{{ $pageCounter }}">				               			
						<div class="container">
						   <div class="row">
							  <div class="col-xl-10 col-lg-10 col-md-12 blog-main">
								 <div class="page-header">
									 <div class="page-header-txt page-header-txt{{ $pageCounter }}">{{$page->title}}</div>

									 @if ($page->thumbnail != "")
										<div class="page-header-img">
										   <img src="{{ $page->thumbnail }}" alt="{{ $page->title }}">
										</div>
									 @endif
								 </div>

							  </div><!-- /.col -->
							</div><!-- /.row -->
						</div><!-- /.container -->
					</div><!-- /.page-header-background -->
			    </div><!-- /.page-header -->
	        
		        <!-- Content -->
			    <div class="page-content page-content{{ $pageCounter }}">	
					<div class="container">
						<div class="row">
							<div class="col-xl-10 col-lg-10 col-md-12 blog-main">
								<div class="blog-post">							   								
								   {!! $page->body !!}	
								   
								   @if ($pageCounter == 10)  					        
						              <div class="lasik-end"><div class="btn-download"><a data-toggle="modal" href="#modalDownload">Download the guide here</a></div></div>					
						           @endif
						
								</div><!-- /.blog-post -->                   
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.container -->
					
					<div class="page-content-background">
				        @if ($pageCounter == 6)  					        
						    <img src="{{ url('') }}/images/site/16-possible-side-effects.jpg">						
						@endif												
						
						@if ($pageCounter == 10)  					        
						    <img src="{{ url('') }}/images/site/26-end.jpg">						
						@endif
						
					</div>		
				</div><!-- /.page-content -->
				
			</div><!-- /.blog-masthead -->                    
      @endif			  		  	  	  			  	
      
      @if ($page->popup_type != "")  
          @foreach ($popups as $popup)   
             if ($popup->id == $page->popup_type)
                @include('site/partials/popup') 
             endif
          @endforeach
      @endif
         			  						  
   @endforeach

   <div class="btn-download-mobile"><a data-toggle="modal" href="#modalDownload">Download the guide here</a></div>
   
@endif 

<!-- The Modal -->
<div class="modal fade modal-Download" id="modalDownload">
  <div class="modal-dialog modal-lg modal-dialog-centered">	     
      
      <div class="modal-content">
      <div class="container no-gutters">      	 	 
      	 <div class="row no-gutters">     	    
     	    
      	    <div class="col-lg-4">
     	         <div class ="div-overlay"> 
					<div class = "div-overlay-top">
					   <img class="guide-popup-img" src="{{ url('') }}/images/site/LASIK-guide-cover.png" title="The Ultimate Australian Guide to LASIK" alt="The Ultimate Australian Guide to LASIK">
					   <img class="guide-popup-img-resp" src="{{ url('') }}/images/site/LASIK-guide-cover-mobile.png" title="The Ultimate Australian Guide to LASIK" alt="The Ultimate Australian Guide to LASIK">
					</div> 					
				</div>  
      	    </div><!-- .col-lg-4 -->
      	    
      	    <div class="col-lg-8">
     	    	 <div class="modal-content-txt">     	    	 
     	    	 
      	    	 <!-- Modal Header -->
				  <div class="modal-header">
				     <div class="modal-btn">
					     <a class="btnMore" href='#' data-dismiss="modal"><img src="{{ url('') }}/images/site/cross.png" title="Close" alt="Close"></a>
				     </div>   
				
					<div class="modal-header-content">
					   <div class="modal-header-content-header">		  						  
						  <div class="modal-header-content-header2">Download The Ultimate Australian Guide to LASIK</div>
						  <div class="modal-header-content-p">
						     <p>Everything you ever wanted to know about LASIK in this exclusive 20-page guide – totally free. </p>
						     <p>Get your hands on it below…</p>
						  </div> 
					   </div>
					</div>																			
				  </div>

				  <!-- Modal body -->
				  <div class="modal-body">
					<div class="modal-body-body">

					   <!-- HubSpot Form - START -->
						<!--[if lte IE 8]>
						<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
						<![endif]-->
						<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>   
						<script>
						  hbspt.forms.create({
							portalId: "5064462",
							formId: "9e2c3b23-30f2-4380-bd55-1d76fd86e7d8"
						});
						</script>
						<!-- HubSpot Form - END -->		


					</div><!-- .modal-body-body -->
				  </div><!-- .modal-body -->
				  </div><!-- .modal-content-txt -->
      	    </div><!-- .col-lg-9 -->
		  </div><!-- .row -->
		</div><!-- .containter -->        	          	
      </div><!-- .modal-content --> 
      </div><!-- .modal-dialog -->   	           
	</div><!-- .modal -->

       
@endsection

@section('scripts')    
    <script src="{{ asset('js/site/lasik-guide-number-counter.js') }}"></script>
@endsection

@section('inline-scripts')
    <script type="text/javascript">
      $( document ).ready(function() {
		  if (typeof Storage != "undefined") {
			if (!localStorage.getItem("done")) {
			  setTimeout(function() {
				$('#modalDownload').modal('show');
			  }, 180000);
			}
			localStorage.setItem("done", true);
		  }
		});
		
		if( $('.lasik-guide-overview').length ) {	
			var a = 0;
			$(window).scroll(function() {         
				  var oTop = $('.lasik-guide-overview').offset().top - window.innerHeight;
				  if (a == 0 && $(window).scrollTop() > oTop) {
                      $( ".lasik-guide-menu" ).fadeIn(3000);
					  $( ".lasik-guide-progressbar" ).fadeIn(3000);
				  }	else  {
					 $( ".lasik-guide-menu" ).fadeOut( "slow" ); 
					 $( ".lasik-guide-progressbar" ).fadeOut( "slow" ); 					  					 
				  }									  
			});				
 	    }						
	   
		function link_onclick(id, position)  {		
			$(".lasik-guide-menu>ul>li>a").removeClass("active");
			$("#link" + id).addClass("active");
			
			for (i=0; i <= {{sizeof($allPages)}}; i++)  {
				if (position >= i)  {
			       $("#bar" + i).addClass("complete");
			    } else  {
				   $("#bar" + i).removeClass("complete");	
			    }
			}					
		}						 		
		
		@php 
	       $pageCounter = 0;
		   $divCounter = 0;
	    @endphp

	    @foreach ($allPages as $page)   
	       @php 
		      $pageCounter++;                                 
	       @endphp
	   
	       if( $('#section{{ $page->id }}').length ) {
				var a = 0;
				$(window).scroll(function() {         
					  var oTop = $('#section{{ $page->id }}').offset().top - window.innerHeight;
					  if (a == 0 && $(window).scrollTop() > oTop) {							 
						 @if ($pageCounter != 1 && $pageCounter != 3 )  
                             @php 
		                        $divCounter++;  
	                         @endphp
		   
						     link_onclick({{ $page->id }}, {{ $divCounter }});
						 @endif
					  }									  
				});				
			}
		@endforeach	
								 
    </script>
@endsection
