<div class="container">
    <div class="row">
        <div class="col-sm-12">

            <ul class="nav nav-tabs nav-fill" id="savings-calculator" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="glasses-tab" data-toggle="tab" href="#glasses" role="tab"
                       aria-controls="glass" aria-selected="true">
                        <img src="{{ asset('images/site/icon-specs.png') }}" alt="specs"/>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="contact-lenses-tab" data-toggle="tab" href="#contact-lenses" role="tab"
                       aria-controls="lens" aria-selected="false">
                        <img src="{{ asset('images/site/icon-contacts.png') }}" alt="specs"/>
                    </a>
                </li>
            </ul>
            <div class="tab-content" id="savings-calculator-content">
                <div class="tab-pane fade show active" id="glasses" role="tabpanel" aria-labelledby="glasses-tab">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-sm-6 blog-post">
                                <h2>How much do you spend on glasses each year?</h2>
                                Drag the slider to enter the figure</br>

                                <div class="d-none d-sm-block">
                                    <div class="icon-container">
                                        <i class="fas fa-question-circle info-popover"></i>
                                    </div>

                                    <div class="popover-container">
                                        <div class="popover bs-popover-right show" x-placement="right"
                                             style="position: relative; top: 0px; left: 0px;">
                                            <div class="arrow"></div>
                                            <div class="popover-body">Most people spend $200 each year on a single pair
                                                of glasses” and for the contacts icon.
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-12 col-sm-6 slider-container">
                                <div class="row no-gutters">
                                    <div class="col-12 slider-heading">Cost of glasses</div>
                                    <div class="col-12">
                                        <div class="row no-gutters">
                                            <div class="col-10">
                                                <input id="cost-of-glasses" type="range" min="0" max="1000" step="5"
                                                       value="200">
                                            </div>
                                            <div class="col-2 text-right cost-value">$<span id="cost-of-glasses-value">200</span>
                                            </div>
                                        </div>

                                        <div class="d-block d-sm-none">
                                            <div class="icon-container">
                                                <i class="fas fa-question-circle info-popover"></i>
                                            </div>

                                            <div class="popover-container">
                                                <div class="popover bs-popover-right show" x-placement="right"
                                                     style="position: relative; top: 0px; left: 0px;">
                                                    <div class="arrow"></div>
                                                    <div class="popover-body">Most people spend $200 each year on a
                                                        single pair of glasses” and for the contacts icon.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="contact-lenses" role="tabpanel" aria-labelledby="contact-lenses-tab">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-sm-6 blog-post">
                                <h2>How much do you spend on contacts each year?</h2>
                                Drag the slider to enter the figure</br>

                                <div class="d-none d-sm-block">
                                    <div class="icon-container">
                                        <i class="fas fa-question-circle info-popover"></i>
                                    </div>

                                    <div class="popover-container">
                                        <div class="popover bs-popover-right show" x-placement="right"
                                             style="position: relative; top: 0px; left: 0px;">
                                            <div class="arrow"></div>
                                            <div class="popover-body">Most people spend $100 a year on
                                                visiting their optometrist, $30 a box of lenses which equates to $258
                                                per year on lenses and $150 a year on cleaning solution.
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-12 col-sm-6 slider-container">
                                <div class="row no-gutters ">
                                    <div class="col-12 slider-heading">Consultation Fee</div>
                                    <div class="col-12">
                                        <div class="row no-gutters">
                                            <div class="col-10">
                                                <input id="consultation-fee" type="range" min="0" max="1000" step="5"
                                                       value="100">
                                            </div>
                                            <div class="col-2 text-right cost-value">$<span id="consultation-fee-value">100</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row no-gutters slider-container">
                                    <div class="col-12 slider-heading">Lenses</div>
                                    <div class="col-12">
                                        <div class="row no-gutters">
                                            <div class="col-10">
                                                <input id="lenses" type="range" min="0" max="1000" step="5" value="250">
                                            </div>
                                            <div class="col-2 text-right cost-value">$<span id="lenses-value">250</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row no-gutters slider-container">
                                    <div class="col-12 slider-heading">Cleaning Solution</div>
                                    <div class="col-12">
                                        <div class="row no-gutters">
                                            <div class="col-10">
                                                <input id="cleaning-solution" type="range" min="0" max="1000" step="5"
                                                       value="150">
                                            </div>
                                            <div class="col-2 text-right cost-value">$<span
                                                        id="cleaning-solution-value">150</span></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-block d-sm-none">
                                    <div class="icon-container">
                                        <i class="fas fa-question-circle info-popover"></i>
                                    </div>

                                    <div class="popover-container">
                                        <div class="popover bs-popover-right show" x-placement="right" style="position: relative; top: 0px; left: 0px;">
                                            <div class="arrow"></div>
                                            <div class="popover-body">Most people spend $100 a year on
                                                visiting their optometrist, $30 a box of lenses which equates to $258
                                                per year on lenses and $150 a year on cleaning solution.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="calculator-result">
                <div class="container">
                    <div class="row text-center justify-content-between">
                        <div class="col-12 col-sm-6 align-self-center order-last order-sm-first">

                            <div class="row">
                                <div class="col-12 col-sm-6 col-md-5 align-self-center">
                                    <div class="input-group cps-container">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">$</span>
                                        </div>
                                        <input id="cps" type="number" class="form-control cps-field" value="4400">
                                    </div>
                                </div>

                                <div class="col-12 col-sm-6 col-md-5 align-self-center">
                                    <span class="cps-desc"> Cost per surgery</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-3 col-md-4 col-lg-3 pull-right">Estimated savings</br><span
                                    class="result-container">$<span id="result">16,600</span></span></br>over 30 years
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 text-center">
            <a href="{{ url('pages/costs/payment-options') }}" role="button" class="bpo-button">BROWSE PAYMENT OPTIONS </br> <span>From As Little As $30 Per Week</span></a>
        </div>
    </div>

    <div class="row logo-block-container">
        <div class="col-12 col-sm-12 col-md-4 text-center">
            <div class="logo-block one">
                <img src="{{ asset('images/site/icon-LifetimeVisionProgram.png') }}">
                <p>Our Lifetime of Vision program provides you with the confidence and reassurance that if sometime in the future a re-treatment becomes appropriate to maintain your agreed visual goal, (either distance or near), we will provide this to you at no extra charge.</p>
            </div>
        </div>
        <div class="col-12 col-sm-12 col-md-4 text-center logo-block-two-container">
            <div class="logo-block two">
                <img src="{{ asset('images/site/icon-Aus1stSatisfactionGurantee.png') }}">
                <p>We’re proud to say that our patient satisfaction rating is considered a benchmark in the industry. More than 98% of our patients are completely satisfied after treatment. If after exhausting all possible methods of correcting your vision you are not satisfied, and we have not met the agreed visual goal, personalEYES will refund the cost of that procedure</p>
            </div>
        </div>
        <div class="col-12 col-sm-12 col-md-4 text-center">
            <div class="logo-block three">
                <img src="{{ asset('images/site/icon-PriceGuarantee.png') }}">
                <p>We will match any publicly advertising price or formal quotation; based on the same procedure and inclusions. In some cases we may require verification of the procedure and/or inclusions.</p>
                <p>One price—we won’t hide any costs—no extra charges for tests, medications or follow-up visits.</p>
            </div>
        </div>
    </div>
</div>