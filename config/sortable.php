<?php

return [
    'entities' => [
        'modules' => '\App\Module',
        'documents' => '\App\Document',
        'document-categories' => '\App\DocumentCategory',
        'faqs' => '\App\Faq',
        'faq-categories' => '\App\FaqCategory',
        'gallery-categories' => '\App\GalleryCategory',
        'member-types' => '\App\MemberType',
        'news-categories' => '\App\NewsCategory',
    	'pages' => '\App\Page',
    	'page-categories' => '\App\PageCategory',
    	'projects' => '\App\Project',
    	'project-categories' => '\App\ProjectCategory',
        'team-members' => '\App\TeamMember',
        'team-categories' => '\App\TeamCategory',
        'properties' => '\App\Properties',
        'property-categories' => '\App\PropertyCategory',
        // 'articles' => '\Article' for simple sorting (entityName => entityModel) or
        // 'posts' => ['entity' => '\Post', 'relation' => 'tags'] for many to many or many to many polymorphic relation sorting
    ],
];
