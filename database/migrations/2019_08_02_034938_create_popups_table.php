<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePopupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('popups', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';

            $table->increments('id');    
			$table->text('title');
            $table->text('form')->nullable();		
			$table->string('image')->nullable();
			$table->string('popup_position')->nullable();
            $table->enum('status', ['active','passive'])->default('passive'); 
			$table->enum('is_deleted', ['true','false'])->default('false');
            $table->timestamps();
        });			
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
