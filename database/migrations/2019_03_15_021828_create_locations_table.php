<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';

            $table->increments('id');             
            $table->string('name');  
			$table->string('slug');
			$table->string('description')->nullable();		
            $table->text('address')->nullable();    
			$table->text('address2')->nullable();    
			$table->text('suburb')->nullable();    
			$table->text('state')->nullable();    
			$table->text('postcode')->nullable();
			$table->text('country')->nullable();    
			$table->text('phone')->nullable();    
			$table->text('mobile')->nullable();    
			$table->text('email')->nullable(); 
			$table->text('fax')->nullable(); 
			$table->text('website')->nullable();   
			$table->string('fileName')->nullable();		
			$table->string('map')->nullable();	
			$table->string('directions')->nullable();		
            $table->enum('status', ['active','passive'])->default('passive');
            $table->integer('position');
			$table->enum('is_deleted', ['true','false'])->default('false');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
