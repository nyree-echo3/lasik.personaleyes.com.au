<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';

            $table->increments('id');
            $table->integer('category_id');
            $table->string('title');
            $table->string('slug');
            $table->text('meta_title');
            $table->text('meta_keywords');
            $table->text('meta_description');
            $table->string('property_type')->nullable();
            $table->string('new_or_established')->nullable();
            $table->integer('lead_agent_id')->nullable();
            $table->integer('dual_agent_id')->nullable();
            $table->string('authority')->nullable();
            $table->decimal('rental_per_week', 65, 2)->nullable();
            $table->decimal('rental_per_month', 65, 2)->nullable();
            $table->decimal('security_bond', 65, 2)->nullable();
            $table->decimal('price', 65, 2)->nullable();
            $table->string('price_display')->nullable();
            $table->string('price_text')->nullable();
            $table->date('date_available')->nullable();
            $table->string('landlord_name')->nullable();
            $table->string('landlord_email')->nullable();
            $table->string('landlord_phone_number')->nullable();
            $table->string('unit')->nullable();
            $table->string('street_number')->nullable();
            $table->string('street_name')->nullable();
            $table->string('suburb')->nullable();
            $table->string('municipality')->nullable();
            $table->enum('hide_street_address', ['true','false'])->nullable();
            $table->enum('hide_street_view', ['true','false'])->nullable();
            $table->integer('bedrooms')->nullable();
            $table->integer('bathrooms')->nullable();
            $table->integer('ensuites')->nullable();
            $table->integer('toilets')->nullable();
            $table->integer('garage_spaces')->nullable();
            $table->integer('carport_spaces')->nullable();
            $table->integer('open_spaces')->nullable();
            $table->integer('living_areas')->nullable();
            $table->integer('house_size')->nullable();
            $table->string('house_size_unit')->nullable();
            $table->integer('land_size')->nullable();
            $table->string('land_size_unit')->nullable();
            $table->string('enery_efficiency_rating')->nullable();
            $table->text('outdoor_features')->nullable();
            $table->text('indoor_features')->nullable();
            $table->text('heating_cooling')->nullable();
            $table->text('eco_friendly_features')->nullable();
            $table->string('other_features')->nullable();
            $table->text('headline')->nullable();
            $table->text('description')->nullable();
            $table->string('statement_of_information_pdf')->nullable();
            $table->text('inspections_data')->nullable();
            $table->enum('status', ['active','passive'])->default('passive');
            $table->integer('position');
            $table->enum('is_deleted', ['true','false'])->default('false');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
