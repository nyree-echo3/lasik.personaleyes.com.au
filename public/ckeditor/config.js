/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
    // Define changes to default configuration here.
    // For complete reference see:
    // http://docs.ckeditor.com/#!/api/CKEDITOR.config

    CKEDITOR.plugins.addExternal('dialogui', '/ckeditor/plugins/dialogui/', 'plugin.js');
    CKEDITOR.plugins.addExternal('dialogadvtab', '/ckeditor/plugins/dialogadvtab/', 'plugin.js');
    CKEDITOR.plugins.addExternal('basicstyles', '/ckeditor/plugins/basicstyles/', 'plugin.js');
    CKEDITOR.plugins.addExternal('bidi', '/ckeditor/plugins/bidi/', 'plugin.js');
    CKEDITOR.plugins.addExternal('blockquote', '/ckeditor/plugins/blockquote/', 'plugin.js');
    CKEDITOR.plugins.addExternal('button', '/ckeditor/plugins/button/', 'plugin.js');
    CKEDITOR.plugins.addExternal('panelbutton', '/ckeditor/plugins/panelbutton/', 'plugin.js');
    CKEDITOR.plugins.addExternal('panel', '/ckeditor/plugins/panel/', 'plugin.js');
    CKEDITOR.plugins.addExternal('floatpanel', '/ckeditor/plugins/floatpanel/', 'plugin.js');
    CKEDITOR.plugins.addExternal('contextmenu', '/ckeditor/plugins/contextmenu/', 'plugin.js');
    CKEDITOR.plugins.addExternal('div', '/ckeditor/plugins/div/', 'plugin.js');
    CKEDITOR.plugins.addExternal('resize', '/ckeditor/plugins/resize/', 'plugin.js');
    CKEDITOR.plugins.addExternal('toolbar', '/ckeditor/plugins/toolbar/', 'plugin.js');
    CKEDITOR.plugins.addExternal('elementspath', '/ckeditor/plugins/elementspath/', 'plugin.js');
    CKEDITOR.plugins.addExternal('enterkey', '/ckeditor/plugins/enterkey/', 'plugin.js');
    CKEDITOR.plugins.addExternal('entities', '/ckeditor/plugins/entities/', 'plugin.js');
    CKEDITOR.plugins.addExternal('popup', '/ckeditor/plugins/popup/', 'plugin.js');
    CKEDITOR.plugins.addExternal('filebrowser', '/ckeditor/plugins/filebrowser/', 'plugin.js');
    CKEDITOR.plugins.addExternal('find', '/ckeditor/plugins/find/', 'plugin.js');
    CKEDITOR.plugins.addExternal('font', '/ckeditor/plugins/font/', 'plugin.js');
    CKEDITOR.plugins.addExternal('fakeobjects', '/ckeditor/plugins/fakeobjects/', 'plugin.js');
    CKEDITOR.plugins.addExternal('floatingspace', '/ckeditor/plugins/floatingspace/', 'plugin.js');
    CKEDITOR.plugins.addExternal('listblock', '/ckeditor/plugins/listblock/', 'plugin.js');
    CKEDITOR.plugins.addExternal('richcombo', '/ckeditor/plugins/richcombo/', 'plugin.js');
    CKEDITOR.plugins.addExternal('forms', '/ckeditor/plugins/forms/', 'plugin.js');
    CKEDITOR.plugins.addExternal('format', '/ckeditor/plugins/format/', 'plugin.js');
    CKEDITOR.plugins.addExternal('horizontalrule', '/ckeditor/plugins/horizontalrule/', 'plugin.js');
    CKEDITOR.plugins.addExternal('htmlwriter', '/ckeditor/plugins/htmlwriter/', 'plugin.js');
    CKEDITOR.plugins.addExternal('iframe', '/ckeditor/plugins/iframe/', 'plugin.js');
    CKEDITOR.plugins.addExternal('wysiwygarea', '/ckeditor/plugins/wysiwygarea/', 'plugin.js');
    CKEDITOR.plugins.addExternal('indent', '/ckeditor/plugins/indent/', 'plugin.js');
    CKEDITOR.plugins.addExternal('indentblock', '/ckeditor/plugins/indentblock/', 'plugin.js');
    CKEDITOR.plugins.addExternal('indentlist', '/ckeditor/plugins/indentlist/', 'plugin.js');
    CKEDITOR.plugins.addExternal('justify', '/ckeditor/plugins/justify/', 'plugin.js');
    CKEDITOR.plugins.addExternal('menubutton', '/ckeditor/plugins/menubutton/', 'plugin.js');
    CKEDITOR.plugins.addExternal('list', '/ckeditor/plugins/list/', 'plugin.js');
    CKEDITOR.plugins.addExternal('liststyle', '/ckeditor/plugins/liststyle/', 'plugin.js');
    CKEDITOR.plugins.addExternal('maximize', '/ckeditor/plugins/maximize/', 'plugin.js');
    CKEDITOR.plugins.addExternal('pagebreak', '/ckeditor/plugins/pagebreak/', 'plugin.js');
    CKEDITOR.plugins.addExternal('pastetext', '/ckeditor/plugins/pastetext/', 'plugin.js');
    CKEDITOR.plugins.addExternal('preview', '/ckeditor/plugins/preview/', 'plugin.js');
    CKEDITOR.plugins.addExternal('print', '/ckeditor/plugins/print/', 'plugin.js');
    CKEDITOR.plugins.addExternal('removeformat', '/ckeditor/plugins/removeformat/', 'plugin.js');
    CKEDITOR.plugins.addExternal('selectall', '/ckeditor/plugins/selectall/', 'plugin.js');
    CKEDITOR.plugins.addExternal('showblocks', '/ckeditor/plugins/showblocks/', 'plugin.js');
    CKEDITOR.plugins.addExternal('showborders', '/ckeditor/plugins/showborders/', 'plugin.js');
    CKEDITOR.plugins.addExternal('sourcearea', '/ckeditor/plugins/sourcearea/', 'plugin.js');
    CKEDITOR.plugins.addExternal('tab', '/ckeditor/plugins/tab/', 'plugin.js');
    CKEDITOR.plugins.addExternal('undo', '/ckeditor/plugins/undo/', 'plugin.js');
    CKEDITOR.plugins.addExternal('widgetselection', '/ckeditor/plugins/widgetselection/', 'plugin.js');
    CKEDITOR.plugins.addExternal('lineutils', '/ckeditor/plugins/lineutils/', 'plugin.js');
    CKEDITOR.plugins.addExternal('widget', '/ckeditor/plugins/widget/', 'plugin.js');
    CKEDITOR.plugins.addExternal('mjTab', '/ckeditor/plugins/mjTab/', 'plugin.js');
    CKEDITOR.plugins.addExternal('juiAccordion', '/ckeditor/plugins/juiAccordion/', 'plugin.js');

    config.filebrowserBrowseUrl = base_url + '/components/ckfinder/ckfinder.html?type=Media';
    config.filebrowserImageBrowseUrl = base_url + '/components/ckfinder/ckfinder.html?type=Media';
    config.filebrowserFlashBrowseUrl = base_url + '/components/ckfinder/ckfinder.html?type=Media';
    config.filebrowserUploadUrl = base_url + '/components/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Media';
    config.filebrowserImageUploadUrl = base_url + '/components/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Media';
    config.filebrowserFlashUploadUrl = base_url + '/components/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Media';

    // The configuration options below are needed when running CKEditor from source files.
    config.plugins = 'dialogui,dialog,about,a11yhelp,dialogadvtab,basicstyles,bidi,blockquote,clipboard,button,panelbutton,panel,floatpanel,menu,contextmenu,div,resize,toolbar,elementspath,enterkey,entities,popup,filebrowser,find,fakeobjects,floatingspace,font,listblock,richcombo,forms,format,horizontalrule,htmlwriter,iframe,wysiwygarea,image,indent,indentblock,indentlist,justify,menubutton,link,list,liststyle,magicline,maximize,pagebreak,pastetext,pastefromword,preview,print,removeformat,selectall,showblocks,showborders,sourcearea,specialchar,scayt,tab,table,tabletools,undo,wsc,widgetselection,lineutils,widget,mjTab,juiAccordion';
    // %REMOVE_END%

    config.language = 'en-au';
    config.uiColor = '#C0C0C0';
    config.height = '25em';
    config.forcePasteAsPlainText = true;
    config.colorButton_enableMore = false;
    config.colorButton_enableAutomatic = false;

    // The toolbar groups arrangement, optimized for two toolbar rows.
    config.toolbarGroups = [
//		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ,'Templates'] },
//		{ name: 'clipboard',   groups: [ 'Cut','Copy','PasteText','-','Undo','Redo','-','SpellChecker', 'Scayt'  ] },
//		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
//		{ name: 'links' },
//		{ name: 'insert' ,      groups : [ 'Image','Iframe','Table','HorizontalRule' ] },
//		{ name: 'forms' },
//		{ name: 'tools' },
//
//		{ name: 'others' },
//		'/',
//		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
//		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
//		{ name: 'styles' },
//		{ name: 'colors' },
        { name: 'document',    groups : [ 'mode','-','Save','NewPage','DocProps','Preview','Print','-','Templates' ] },
        { name: 'clipboard',   groups : [ 'Cut','Copy','PasteText','-','Undo','Redo','-','SpellChecker', 'Scayt'  ] },
        { name: 'links',       groups : [ 'Link','Unlink','Anchor' ] },
        { name: 'insert',      groups : [ 'Image','JUIAccordion','MJTab','Table','HorizontalRule' ] },
        '/',
        { name: 'basicstyles', groups : [ 'Bold','Italic','Underline','-','RemoveFormat' ] },
        { name: 'paragraph',   groups : [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
        //{ name: 'paragraph2',   groups : [ 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
        { name: 'styles',      groups : [ 'Format','FontSize' ] },


    ];

    // Remove some buttons provided by the standard plugins, which are
    // not needed in the Standard(s) toolbar.
    config.removeButtons = 'Subscript,Superscript,Strike,Paste,PasteFromWord';

    // Set the most common block elements.
    //config.format_tags = 'p;h1;h2;h3;pre';
    config.format_tags = 'p;h1;h2;h3;h4;h5';

    // Simplify the dialog windows.
    config.removeDialogTabs = 'image:advanced;link:advanced';
    config.extraAllowedContent = 'iframe[*];*[class,id];';
    config.allowedContent = true;

    //For juiAccordion
    config.mjTab_managePopupTitle = false;
    config.mjTab_managePopupContent = true;
    config.juiAccordion_managePopupContent = true;
    config.mj_variables_allow_html = false;
    //config.contentsCss = '/components/jquery-ui/themes/base/all.css';
    //config.contentsCss = '/ckeditor/plugins/mjTab/frontend/mjTab.css';
    config.contentsCss =  ['/components/jquery-ui/themes/base/all.css', '/ckeditor/plugins/mjTab/frontend/mjTab.css'];

};
