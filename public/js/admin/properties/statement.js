function openStatementPopup() {

    CKFinder.popup({
        chooseFiles: true,
        onInit: function (finder) {
            finder.on('files:choose', function (evt) {
                var file = evt.data.files.first();
                $('#added_document').html('<a href="' + base_url + file.getUrl() + '">' + base_url + file.getUrl() + "</a>");
                $('#remove-document').removeClass('invisible');
                $('#statement_of_information_pdf').val(file.getUrl());

            });
//                    finder.on( 'file:choose:resizedImage', function( evt ) {
//                        $('#added_image').html('<image src="'+base_url+evt.data.resizedUrl+'">');
//                        $('#remove-image').removeClass('invisible');
//                        $('#thumbnail').val(evt.data.resizedUrl);
//                    } );
        }
    });
}