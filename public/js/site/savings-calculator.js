$( document ).ready(function() {

    $('[data-toggle="popover"]').popover();

    $('#cost-of-glasses').rangeslider({
        polyfill: false,
        onSlide: function(position, value) {
            $('#cost-of-glasses-value').html(value);
            calculate();
        }
    });

    $('#consultation-fee').rangeslider({
        polyfill: false,
        onSlide: function(position, value) {
            $('#consultation-fee-value').html(value);
            calculate();
        }
    });

    $('#lenses').rangeslider({
        polyfill: false,
        onSlide: function(position, value) {
            $('#lenses-value').html(value);
            calculate();
        }
    });

    $('#cleaning-solution').rangeslider({
        polyfill: false,
        onSlide: function(position, value) {
            $('#cleaning-solution-value').html(value);
            calculate();
        }
    });

    $("#cps").keyup(function() {
        calculate();
    });

    $("#cps").change(function() {
        calculate();
    });

    function calculate(){

        var timeframe = 30;
        var surgery_cost = parseInt($('#cps').val());
        var cost_of_glasses = parseInt($('#cost-of-glasses-value').html());
        var consultation_fee = parseInt($('#consultation-fee-value').html());
        var lenses = parseInt($('#lenses-value').html());
        var cleaning_solution = parseInt($('#cleaning-solution-value').html());
        var result = ((cost_of_glasses*timeframe)+(consultation_fee*timeframe)+(lenses*timeframe)+(cleaning_solution*timeframe))-surgery_cost;
        $('#result').html(result.toLocaleString());
    }

    calculate();
});